<?php

// $filename = 'https://financialmodelingprep.com/api/majors-indexes?datatype=json';

$filename = 'financial.json';

$source = json_decode(file_get_contents($filename), true);

header("Content-type:application/json");
echo json_encode($source);

?>