<?php

$filename = 'https://examples.opendatasoft.com/api/datasets/1.0/world-heritage-unesco-list';

// $filename = 'unesco.json';

// .htaccess
// php_value execution_time  30
// php_value max_input_time 60
// php_value memory_limit 128M

$output = 'unesco1.json';
// var_dump(filesize($output));

$handle = fopen($filename, "r");
if ($handle) {
  while (($buffer = fgets($handle, 4096)) !== false) {
      file_put_contents($output, $buffer, FILE_APPEND);
      // echo "Output has been saved to file<br/>";
  }

  if (!feof($handle)) {
      echo "Error: unexpected fgets() fail\n";
  }

 
  fclose($handle);
}

$source = json_decode(file_get_contents($filename), true);

header("Content-type:application/json");
echo json_encode($source);

?>