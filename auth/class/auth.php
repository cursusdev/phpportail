<?php

class Auth {

  /* Jeton de confirmation */
  private $token;
  
  /* GETTERS PUBLICS */
  public function getToken() {
    return $this->token;
  }

  public function __construct() {

  }

  public function auth() {
    $urlAuth = 'http://localhost:3000/authenticate';
    $userData = array(
      'username' => 'cursusdev',
      'password' => 2019
    );
    // We format post data as application/x-www-form-urlencoded so make 
    // sure that you tick it under the rest server parser options.
    $userData = http_build_query($userData, '', '&');

    $ch = curl_init($urlAuth);
    $options = array(
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POSTFIELDS => $userData,
      CURLOPT_VERBOSE => true
    );
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    // close connection
    curl_close($ch);
    return $result;
  }


}