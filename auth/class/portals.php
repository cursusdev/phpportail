<?php

class Portals {

  private $data;

  private $eventID;

  private $urlsID;

  public function __construct(&$token) {
    $this->token = $token;
  }

  public function createPortalsSQL($data, &$token) {
    $event = (object)$data;

    $jsonString = json_encode($event, true);
    $urlJson = 'http://localhost:3000/api/portals';
    $header = array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'Authorization: Bearer ' . $token
    );
    $ch = curl_init($urlJson);
    $options = array(
      CURLOPT_HTTPHEADER => $header,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $jsonString,
      CURLOPT_HTTPHEADER => array('Content-type: application/json'),
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true
    );
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);

    curl_close($ch);
    return $result;
  }

  public function getPortalsSQL(&$token) {
    $urlJson = 'http://localhost:3000/api/portals';
    // authentication
    $header = array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'Authorization: Bearer ' . $token
    );
    $ch = curl_init($urlJson);
    $options = array(
      CURLOPT_HTTPHEADER => $header,
      CURLOPT_RETURNTRANSFER => true
    );
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    // close connection
    curl_close($ch);
    return $result;
  }

  public function getPortalsSQLById($eventID, &$token) {
    $urlJson = 'http://localhost:3000/api/portals/' . $eventID;
    // authentication
    $header = array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'Authorization: Bearer ' . $token
    );
    $ch = curl_init($urlJson);
    $options = array(
      CURLOPT_HTTPHEADER => $header,
      CURLOPT_RETURNTRANSFER => true
    );
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    // close connection
    curl_close($ch);
    return $result;
  }

  public function getPortalsSQLByUrlsId($urlsID, &$token) {
    $urlJson = 'http://localhost:3000/api/portals/?urlsID=' . $urlsID;
    // authentication
    $header = array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'Authorization: Bearer ' . $token
    );
    $ch = curl_init($urlJson);
    $options = array(
      CURLOPT_HTTPHEADER => $header,
      CURLOPT_RETURNTRANSFER => true
    );
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    // close connection
    curl_close($ch);
    return $result;
  }

  public function deleteEventSQL($eventID, &$token) {
    $urlJson = 'http://localhost:3000/api/portals/' . $eventID;
    // authentication
    $header = array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'Authorization: Bearer ' . $token
    );
    $json = '';
    $ch = curl_init($urlJson);
    $options = array(
      CURLOPT_HTTPHEADER => $header,
      CURLOPT_CUSTOMREQUEST => "DELETE",
      CURLOPT_POSTFIELDS => $json,
      CURLOPT_RETURNTRANSFER => true
    );
    // Setting curl options
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    $result = json_decode($result);
    // close connection
    curl_close($ch);
    return $result;
  }

  public function updatePortalsSQLById($eventID, $data, &$token) {
    $req = json_encode(json_decode($this->getPortalsSQLById($eventID, $token)));
    if ($req != '{}') {
      $createdAt = json_decode($req)->{'eventCreated_at'};
      $urlName = json_decode($req)->{'urlName'};
      $intervalEv = json_decode($req)->{'intervalEv'};
      $newUrl = json_decode($req)->{'newUrl'};
      $beginEv = json_decode($req)->{'beginEv'};
      $endEv = json_decode($req)->{'endEv'};
      $urlsIDFK = json_decode($req)->{'urlsIDFK'};
      $dataJson = json_decode($req)->{'dataJson'};
        $arr = array(
          'eventCreated_at' => $createdAt,
          'urlName' => $urlName,
          'intervalEv' => $intervalEv,
          'newUrl' => $newUrl,
          'beginEv' => $beginEv,
          'endEv' => $endEv,
          'statusEv' => $data['statusEv'],
          'urlsIDFK' => $urlsIDFK,
          'dataJson' => $dataJson
        );
      $event = (object)$arr;

      $jsonString = json_encode($event, true);
      $urlJson = 'http://localhost:3000/api/portals/' . $eventID;
      // authentication
      $header = array(
        'Accept: application/json',
        'Content-Type: application/x-www-form-urlencoded',
        'Authorization: Bearer ' . $token
      );
      $ch = curl_init($urlJson);
      $options = array(
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_CUSTOMREQUEST => "PUT",
        CURLOPT_POSTFIELDS => $jsonString,
        // CURLOPT_USERPWD => $username . ':' . $password,  // authentication
        CURLOPT_HTTPHEADER => array('Content-type: application/json'),
        CURLOPT_RETURNTRANSFER => true
      );
      curl_setopt_array($ch, $options);
      $result = curl_exec($ch);
      // close connection
      curl_close($ch);
      return $result;
    };
  }

}
