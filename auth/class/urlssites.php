<?php

class urlsSites {

    private $data;

    private $urlsID;

    /* Jeton de confirmation par email */
    private $token;

    public function __construct(&$token) {
      $this->token = $token;
    }

    public function getUrlsIDSQL(&$token) {
      $urlJson = 'http://localhost:3000/api/urlssites';
      // authentication
      $header = array(
        'Accept: application/json',
        'Content-Type: application/x-www-form-urlencoded',
        'Authorization: Bearer ' . $token
      );
      // var_dump($token);
      $ch = curl_init($urlJson);
      $options = array(
        CURLOPT_HTTPHEADER => $header,
        // CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_RETURNTRANSFER => true
        // CURLOPT_FOLLOWLOCATION => true
      );
      curl_setopt_array($ch, $options);
      $result = curl_exec($ch);
      // close connection
      curl_close($ch);
      return $result;
    }

    public function getLastUrlsID(&$token) {
      $urlJson = 'http://localhost:3000/api/urlssites/lastID';
      // authentication
      $header = array(
        'Accept: application/json',
        'Content-Type: application/x-www-form-urlencoded',
        'Authorization: Bearer ' . $token
      );
      $ch = curl_init($urlJson);
      $options = array(
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_RETURNTRANSFER => true
      );
      curl_setopt_array($ch, $options);
      $result = curl_exec($ch);
      // close connection
      curl_close($ch);
      return $result;
    }

    public function createUrlsSiteSQL($data, &$token) {
      $event = (object)$data;
      $jsonString = json_encode($event, true);
      $urlJson = 'http://localhost:3000/api/urlssites';
      $header = array(
        'Accept: application/json',
        'Content-Type: application/x-www-form-urlencoded',
        'Authorization: Bearer ' . $token
      );
      $ch = curl_init($urlJson);
      $options = array(
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $jsonString,
        CURLOPT_HTTPHEADER => array('Content-type: application/json'),
        CURLOPT_RETURNTRANSFER => true
      );
      curl_setopt_array($ch, $options);
      $result = curl_exec($ch);

      curl_close($ch);
      return $result;
    }

    public function updateUrlSQLById($urlsID, $data, &$token) {
      $req = json_encode(json_decode($this->getUrlsIDSQLById($urlsID, $token)));
      if ($req !== '{}') {
        $res = json_decode(json_decode($req)->{'urls'}); 
        $sumUrl = json_decode(json_decode($req)->{'sumUrls'});
        $urlsCreatedAt = json_decode($req)->{'urlsCreated_at'};
        $sumUrlsEv = json_decode(json_decode($req)->{'sumUrlsEv'});
        $siteIDFK = json_decode(json_decode($req)->{'siteIDFK'});
        $arr = array(
          'urlsCreated_at' => $urlsCreatedAt,
          'urls' => json_decode('{"url":"' . $res->{'url'} .'","urlCreated_at":"' . $res->{'urlCreated_at'} . '","beginDate":"' . $res->{'beginDate'} . '","endDate":"' . $res->{'endDate'} . '","interval":"' . $data['interval'] . '","totEv":' . $data['totEv'] . ',"urlStatus":' . $data['urlStatus'] . '}'),
          'sumUrls' => $sumUrl + 1,
          'sumUrlsEv' => $sumUrlsEv,
          'siteIDFK' => $siteIDFK
        );
        $event = (object)$arr;

        $jsonString = json_encode($event, true);
        $urlJson = 'http://localhost:3000/api/urlssites/' . $urlsID;
        // authentication
        $header = array(
          'Accept: application/json',
          'Content-Type: application/x-www-form-urlencoded',
          'Authorization: Bearer ' . $token
        );
        $ch = curl_init($urlJson);
        $options = array(
          CURLOPT_HTTPHEADER => $header,
          CURLOPT_CUSTOMREQUEST => "PUT",
          CURLOPT_POSTFIELDS => $jsonString,
          // CURLOPT_USERPWD => $username . ':' . $password,  // authentication
          CURLOPT_HTTPHEADER => array('Content-type: application/json'),
          CURLOPT_RETURNTRANSFER => true
        );
        curl_setopt_array($ch, $options);
        $result = curl_exec($ch);
        // close connection
        curl_close($ch);
        return $result;
      };
    }

    public function getUrlsIDSQLById($urlsID, &$token) {
      $urlJson = 'http://localhost:3000/api/urlssites/' . $urlsID;
      // authentication
      $header = array(
        'Accept: application/json',
        'Content-Type: application/x-www-form-urlencoded',
        'Authorization: Bearer ' . $token
      );
      $ch = curl_init($urlJson);
      $options = array(
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_RETURNTRANSFER => true
      );
      curl_setopt_array($ch, $options);
      $result = curl_exec($ch);
      // close connection
      curl_close($ch);
      return $result;
    }

    public function deleteUrlSQL($urlsID, &$token) {
      $urlJson = 'http://localhost:3000/api/urlssites/' . $urlsID;
      // authentication
      $header = array(
        'Accept: application/json',
        'Content-Type: application/x-www-form-urlencoded',
        'Authorization: Bearer ' . $token
      );
      $json = '';
      $ch = curl_init($urlJson);
      $options = array(
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_CUSTOMREQUEST => "DELETE",
        CURLOPT_POSTFIELDS => $json,
        CURLOPT_RETURNTRANSFER => true
      );
      // Setting curl options
      curl_setopt_array($ch, $options);
      $result = curl_exec($ch);
      $result = json_decode($result);
      // close connection
      curl_close($ch);
      return $result;
    }

  }