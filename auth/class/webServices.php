<?php

class WebServices {

  private $url;

  /* Jeton de confirmation par email */
  private $token;

  public function __construct(&$token) {
    $this->token = $token;
  }

  public function getWebService($urlJson, &$token) {
    // authentication
    $header = array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'Authorization: Bearer ' . $token
    );
    $ch = curl_init($urlJson);
    $options = array(
      CURLOPT_HTTPHEADER => $header,
      CURLOPT_RETURNTRANSFER => true
    );
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    // close connection
    curl_close($ch);
    return $result;
  }

  public function getFinancial(&$token) {
    $urlJson = 'https://financialmodelingprep.com/api/majors-indexes?datatype=json';
    // authentication
    $header = array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'Authorization: Bearer ' . $token
    );
    $ch = curl_init($urlJson);
    $options = array(
      CURLOPT_HTTPHEADER => $header,
      CURLOPT_AUTOREFERER => true,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_URL => $urlJson,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_CONNECTTIMEOUT => 0,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_SSL_VERIFYPEER => false
    );
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    // close connection
    if (curl_error($ch)) {
      return 'error:' . curl_error($ch);
    } else {
      $resultJSON = json_decode($result);
      // var_dump($resultJSON);
    }
    curl_close($ch);
    return $result;
  }

  public function getAstro(&$token) {
    $urlJson = 'http://api.open-notify.org/astros.json';
    // authentication
    $header = array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'Authorization: Bearer ' . $token
    );
    $ch = curl_init($urlJson);
    $options = array(
      CURLOPT_HTTPHEADER => $header,
      CURLOPT_AUTOREFERER => true,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_URL => $urlJson,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_CONNECTTIMEOUT => 0,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_SSL_VERIFYPEER => false
    );
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    // close connection
    if (curl_error($ch)) {
      return 'error:' . curl_error($ch);
    } else {
      $resultJSON = json_decode($result);
      // var_dump($resultJSON);
    }
    curl_close($ch);
    return $result;
  }

  public function getUnesco(&$token) {
    $urlJson = 'https://examples.opendatasoft.com/api/datasets/1.0/world-heritage-unesco-list';
    // authentication
    $header = array(
      'Accept: application/json',
      'Content-Type: application/x-www-form-urlencoded',
      'Authorization: Bearer ' . $token
    );
    $ch = curl_init($urlJson);
    $options = array(
      CURLOPT_HTTPHEADER => $header,
      CURLOPT_AUTOREFERER => true,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_URL => $urlJson,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_CONNECTTIMEOUT => 0,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_SSL_VERIFYPEER => false
    );
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    // close connection
    if (curl_error($ch)) {
      return 'error:' . curl_error($ch);
    } else {
      $resultJSON = json_decode($result);
      // var_dump($resultJSON);
    }
    curl_close($ch);
    return $result;
  }

}
