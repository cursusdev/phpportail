<?php

class Websites {

    private $data;

    private $siteID;

    /* Jeton de confirmation par email */
    private $token;

    public function __construct(&$token) {
      $this->token = $token;
    }

    public function createWebsitesSQL($data, &$token) {
      $event = (object)$data;
      $sumEvents = 0;
      $event->sumEvents = $sumEvents;
      $jsonString = json_encode($event, true);
      $urlJson = 'http://localhost:3000/api/websites'; // InsertWebSitesJson @json
      // authentication
      $header = array(
        'Accept: application/json',
        'Content-Type: application/x-www-form-urlencoded',
        'Authorization: Bearer ' . $token
      );
      $ch = curl_init($urlJson);
      $options = array(
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $jsonString,
        // CURLOPT_USERPWD => $username . ':' . $password,  // authentication
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true
      );
      curl_setopt_array($ch, $options);
      $result = curl_exec($ch);
      // close connection
      curl_close($ch);
      return true;
    }

    public function getWebsitesSQL(&$token) {
      $urlJson = 'http://localhost:3000/api/websites'; // InsertWebSitesJson @json
      // authentication
      $header = array(
        'Accept: application/json',
        'Content-Type: application/x-www-form-urlencoded',
        'Authorization: Bearer ' . $token
      );
      $ch = curl_init($urlJson);
      $options = array(
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_RETURNTRANSFER => true
      );
      curl_setopt_array($ch, $options);
      $result = curl_exec($ch);
      // close connection
      curl_close($ch);
      return $result;
    }

    public function getWebsitesSQLById($siteID, &$token) {
      $urlJson = 'http://localhost:3000/api/websites' . $siteID; // InsertWebSitesJson @json
      // authentication
      $header = array(
        'Accept: application/json',
        'Content-Type: application/x-www-form-urlencoded',
        'Authorization: Bearer ' . $token
      );
      $ch = curl_init($urlJson);
      $options = array(
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_RETURNTRANSFER => true
      );
      curl_setopt_array($ch, $options);
      $result = curl_exec($ch);
      //close connection
      curl_close($ch);
      return $result;
    }

    public function updateWebsitesSQL($siteID, $data, &$token) {
      $event = (object)$data;
      $jsonString = json_encode($event, true);
      $urlJson = 'http://localhost:3000/api/websites/' . $siteID;
      // authentication
      $header = array(
        'Accept: application/json',
        'Content-Type: application/x-www-form-urlencoded',
        'Authorization: Bearer ' . $token
      );
      $ch = curl_init($urlJson);
      $options = array(
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_CUSTOMREQUEST => "PUT",
        CURLOPT_POSTFIELDS => $jsonString,
        // CURLOPT_USERPWD => $username . ':' . $password,  // authentication
        CURLOPT_HTTPHEADER => array('Content-type: application/json'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true
      );
      // Setting curl options
      curl_setopt_array($ch, $options);
      $result = curl_exec($ch);
      //close connection
      curl_close($ch);
      return $event;
    }

    public function deleteWebsiteSQL($siteID, &$token) {
      $urlJson = 'http://localhost:3000/api/websites/' . $siteID;
      // authentication
      $header = array(
        'Accept: application/json',
        'Content-Type: application/x-www-form-urlencoded',
        'Authorization: Bearer ' . $token
      );
      $json = '';
      $ch = curl_init($urlJson);
      $options = array(
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_CUSTOMREQUEST => "DELETE",
        CURLOPT_POSTFIELDS => $json,
        CURLOPT_RETURNTRANSFER => true
      );
      // Setting curl options
      curl_setopt_array($ch, $options);
      $result = curl_exec($ch);
      $result = json_decode($result);
      // close connection
      curl_close($ch);
      return $result;
    }

}
