<?php

require_once 'auth_cookie.php'; 
require_once '../environments/env.php';

$host = Env::get_host();
$path = Env::get_path_auth();
$user_mdp = $_GET['mdp'];

if ($_SESSION['mdp'] == $user_mdp) {
    $_SESSION['newmdp'] == 'success';
    $_SESSION['token'] = 'start';
    $_SESSION['urlNewmdp'] = $host . $path . '/confirm_mdp.php?mdp=6w6loaoe';
    header('location: mdpnew.php');
    exit();
} else {
    $_SESSION['newmdp'] == 'fail';
    header('location: login.php');
    exit();
}

// mdp=6w6loaoe fixe pour url provisoire de confirmation
// http://127.0.0.1:8080/phpportail/auth/confirm_mdp.php?mdp=6w6loaoe

