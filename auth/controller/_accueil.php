<?php

// require_once '../home/header.php';
// require_once 'pdo.php';
// require_once 'utiles.php';


// --------------------------ONGLET ACCUEIL


if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == $path . '/home/accueil.php') {

  // requête toute la base de donnée pour les articles de son utilisateur
  $req = $pdo->prepare('SELECT id, auteur, titre, contenu, dateAjout, dateModif, urlsID, urlName, categorie, imgNom, imgType, imgTaille FROM cards WHERE auteur=:account_name ORDER BY dateModif DESC;');
  $req->execute(array(':account_name' => 'Administrateur'));
  $req->setFetchMode(PDO::FETCH_OBJ);
  $newsAdmin = $req->fetchAll();
  // var_dump($newsAdmin);

  $arrCat = [];
  foreach ($newsAdmin as $key => $value) {
    $res = $newsAdmin[$key]->{'categorie'};
    if (array_search($res, $arrCat) === false) {
      $arrCat[] = $res;
    }
  }


  $idMenu = "choix";
  if (isset($_POST['idMenu'])) {
    $idMenu = htmlspecialchars($_POST['idMenu']);
    // var_dump($_POST['idMenu']);
    if ($idMenu != "choix") {

      $req = $pdo->prepare('SELECT id, auteur, titre, contenu, dateAjout, dateModif, urlsID, urlName, categorie, imgNom, imgType, imgTaille FROM cards WHERE auteur=:account_name AND categorie=:categorie ORDER BY dateModif DESC;');
      $req->execute(array(
        ':account_name' => 'Administrateur',
        'categorie' => $idMenu
      ));
      $req->setFetchMode(PDO::FETCH_OBJ);
      $newsAdmin = $req->fetchAll();
    }
  }

  // Retourne true si le menu sélectionné correspond à $keyToCheck
  // On utilise une fonction anonyme ici, de façon a pouvoir capturer une variable extérieure ($idMenu)
  $menuSelected = function($keyToCheck) use ($idMenu) {
    echo ($idMenu === $keyToCheck ? 'selected' : '');
  };


}


// require_once '../home/footer.php';

?>