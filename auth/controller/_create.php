<?php

// require_once '../home/header.php';
require_once '../class/portals.php';
// require_once 'pdo.php';
// require_once 'utiles.php';


// --------------------------ONGLET CREATE-----------


// requête toute la base de donnée pour les actions modifier et supprimer
$req = $pdo->prepare('SELECT id, auteur, titre, contenu, dateAjout, dateModif, urlsID, urlName, categorie, imgNom, imgType, imgTaille FROM cards ORDER BY dateModif DESC;');
$req->execute();
$req->setFetchMode(PDO::FETCH_OBJ);
$news = $req->fetchAll();
// var_dump($news);

// initialise la page à chaque action - "pourra être dispensé"
$inputAuteur = '';
$inputUrlNom = '';
$inputTitre = '';
$inputContenu = '';
$inputCat = '';
// var_dump($_SESSION['urlNameCard']);

if (isset($_POST['urlsPost']) && isset($_POST['urlName'])) {
  $urlsID  = htmlspecialchars($_POST['urlsPost']);
  unset($_POST['urlsPost']);
  $inputUrlNom = htmlspecialchars($_POST['urlName']);
  unset($_POST['urlName']);
}

if (isset($_POST['ajouter'])) {
  $inputUrlNom = htmlspecialchars($_POST['urlNom']);
  $inputTitre = htmlspecialchars($_POST['titre']);
  $inputContenu = htmlspecialchars($_POST['contenu']);
  $inputCat = htmlspecialchars($_POST['categorie']);
  // var_dump('success');
}

// $_SESSION['urlsIDCard'] = 1430;
// $_SESSION['urlNameCard'] = 'http://api.open-notify.org/astros.json';

/* PAGE ADMIN && UTILISATEURS */

// requête toute la base de donnée pour les actions modifier et supprimer
$req = $pdo->prepare('SELECT id, auteur, titre, contenu, dateAjout, dateModif, urlsID, urlName, categorie, imgNom, imgType, imgTaille FROM cards ORDER BY dateModif DESC;');
$req->execute();
$req->setFetchMode(PDO::FETCH_OBJ);
$news = $req->fetchAll();
// var_dump($news);


// Formulaire d'entrée pour l'admin - articles du sites
if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] === $path . '/home/create.php' && isset($_POST['titre']) && isset($_POST['contenu'])) {

  // Requête d'entrée de l'article dans la base de données
  // if(empty($_SESSION['id']) && isset($_POST['ajouter']) && !empty($_FILES["file"]["name"])) {
  if(empty($_SESSION['id']) && isset($_POST['ajouter'])) {

    $auteur = $_SESSION['username'];
    // $urlsID  = $_SESSION['urlsIDCard'];
    $urlNom = htmlspecialchars($_POST['urlNom']);
    $titre = htmlspecialchars($_POST['titre']);
    $contenu = htmlspecialchars($_POST['contenu']);
    $categorie = htmlspecialchars($_POST['categorie']);
    $dateAjout = date('Y-m-d H:i:s');
    $dateModif = $dateAjout;

    // File upload path
    $targetDir = "../uploads/";
    $fileName = basename($_FILES["file"]["name"]);
    $imgType = $_FILES["file"]["type"];
    $imgTaille = $_FILES["file"]["size"];
    $targetFilePath = $targetDir . $fileName;
    $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

    $allowTypes = array('jpg','png','jpeg','gif');
    if(in_array($fileType, $allowTypes)){
        // Upload file to server
        if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
            // Insert image file name into database
            // $insert = $db->query("INSERT into images (file_name, uploaded_on) VALUES ('".$fileName."', NOW())");
            $imgName = $fileName;
            // if ($insert) {
            //     $statusMsg = "The file ".$fileName. " has been uploaded successfully.";
            // } else {
            //     $statusMsg = "File upload failed, please try again.";
            // }
    //     } else {
    //         $statusMsg = "Sorry, there was an error uploading your file.";
    //     }
    // } else {
    //     $statusMsg = 'Sorry, only JPG, JPEG, PNG, GIF, & PDF files are allowed to upload.';
    // }
        }
    }
    // var_dump($fileName);

    /* Vérifie le contenu non vide */
    $emptyUrlNom = ($urlNom == '') ? '<span style="color:red;">Votre news a besoin d\'un nom d\'Url !</span>' : '';
    $emptyTitre = ($titre == '') ? '<span style="color:red;">Votre news a besoin d\'un titre !</span>' : '';
    $emptyContenu = ($contenu == '') ? '<span style="color:red;">Votre news a besoin d\'un contenu !</span>' : '';
    $emptyCat = ($categorie == '') ? '<span style="color:red;">Votre news a besoin d\'une catégorie !</span>' : '';
    $emptyImage = ($fileName == '') ? '<span style="color:red;">Votre news a besoin d\'une image !</span>' : '';

    $checkUrlNom = $urlNom;
    $checkTitre = $titre;
    $checkContenu = $contenu;
    $checkImage = $fileName;
    $checkCat = $categorie;

    if ($checkUrlNom != '' && $checkTitre != '' && $checkContenu != '' && !empty($auteur) && $checkImage != '' && $checkCat != '') {

    // Supprimer toutes les balises HTML et tous les caractères avec une valeur ASCII
    $newTitre = filter_var($titre, FILTER_SANITIZE_STRING);
    $newContenu = filter_var($contenu, FILTER_SANITIZE_STRING);

      // $urlsID = 1;
      // Insertion des données dans la base de donnée
      $sql1 = 'INSERT INTO cards (auteur, titre, contenu, dateAjout, dateModif, urlsID, urlName, categorie, imgNom, imgType, imgTaille) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
      $req1 = $pdo->prepare($sql1);
      $req1->execute(array($auteur, $newTitre, $newContenu, $dateAjout, $dateModif, (int)$urlsID, $urlNom, $categorie, $fileName, $imgType, $imgTaille));
      // initialisation des données du navigateur
      // unset($_POST);
      // header ('location: espace.php');
    }
  }


  // Requête de modification de l'article dans la base de données
  if ($_SERVER['REQUEST_URI'] === $path . '/home/create.php' && !empty($_SESSION['id']) && isset($_POST['ajouter'])) {
    $id = $_SESSION['id'];
    $inputAuteur = $_SESSION['username'];
    $inputTitre = htmlspecialchars($_POST['titre']);
    $inputContenu = htmlspecialchars($_POST['contenu']);
    $inputCat = htmlspecialchars($_POST['categorie']);
    $inputUrl = htmlspecialchars($_POST['urlNom']);
    $dateModif = date('Y-m-d H:i:s');

    // Supprimer toutes les balises HTML et tous les caractères avec une valeur ASCII
    $newInputTitre = filter_var($inputTitre, FILTER_SANITIZE_STRING);
    $newInputContenu = filter_var($inputContenu, FILTER_SANITIZE_STRING);

    $sql2 = 'UPDATE cards SET auteur=:inputAuteur,titre=:inputTitre,contenu=:inputContenu,dateModif=:dateModif,urlName=:urlName,categorie=:categorie WHERE id=:id;';
    $req2 = $pdo->prepare($sql2);
    $req2->execute(array(
        ':inputAuteur' => $inputAuteur,
        ':inputTitre' => $newInputTitre,
        ':inputContenu' => $newInputContenu,
        ':dateModif' => $dateModif,
        ':urlName' => $inputUrl,
        ':categorie' => $inputCat,
        ':id' => $id
    ));
    // initialisation des données du navigateur
    $inputAuteur = '';
    $inputTitre = '';
    $inputContenu = '';
    $inputCat = '';
    $inputUrl = '';
    unset($_SESSION['id']);
    // unset($_POST);
  }
}


// Récupére les données à modifier vers input sur la même page admin.php
if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == $path . '/home/create.php' && !empty($_POST['newId']) && !empty($_POST['editer'])) {
    $idMod = htmlspecialchars($_POST['newId']);
    $_SESSION['id'] = $idMod;
    if($news) {
        for ($i = 0; $i < count($news); $i++) {
            if ($news[$i]->{'id'} == $idMod) {
                $inputUrl = $news[$i]->{'urlName'};
                $inputAuteur = $news[$i]->{'auteur'};
                $inputTitre = $news[$i]->{'titre'};
                $inputContenu = $news[$i]->{'contenu'};
                $dateAjout = $news[$i]->{'dateAjout'};
                $dateModif = $news[$i]->{'dateModif'};
                $inputCat = $news[$i]->{'categorie'};
                $inputImg = $news[$i]->{'imgNom'};
            }
        }
    }
    
    // unset($_POST);
}

// Supprime un article avec son ID
if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == $path . '/home/create.php' && !empty($_POST['newId']) && !empty($_POST['x'])) {
    $idSup = htmlspecialchars($_POST['newId']);
    $sql3 = 'DELETE FROM cards WHERE id=:id;';
    $req3 = $pdo->prepare($sql3);
    $req3->execute(array(
        ':id' => $idSup
    ));
    // initialisation des données du navigateur
    $inputUrl = '';
    $inputAuteur = '';
    $inputTitre = '';
    $inputContenu = '';
    $inputCat = '';
    $inputImg = '';
    // unset($_POST);
    // Regénère l'affiche - Single Page
    // header('Location: create.php');
}

// Nettoye les données du navigateur
if ($_SERVER['REQUEST_URI'] === $path . '/home/create.php' && isset($_POST['vider'])) {
    $inputUrl = '';
    $inputAuteur = '';
    $inputTitre = '';
    $inputContenu = '';
    $inputCat = '';
    $inputImg = '';
    unset($_SESSION['id']);
    // unset($_POST);
}

// if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == $path . '/home/create.php') {

  /*---------------------SELECTION CATEGORIE---------------- */

  // requête la base de données pour comptabilisé le nombres d'entrée (table)
  $countRows = $pdo->prepare('SELECT COUNT(*) FROM cards WHERE auteur=:account_name;');
  $countRows->execute(array(
    ':account_name' => $_SESSION['username']
  ));
  $countRows->setFetchMode(PDO::FETCH_OBJ);
  $count = $countRows->fetchAll();
  $_SESSION['countRows'] = $count[0]->{'COUNT(*)'};

  $idMenu = "choix";
  if (isset($_POST['idMenu'])) {
    $idMenu = htmlspecialchars($_POST['idMenu']);
    // var_dump($_POST['idMenu']);
    if ($idMenu != "choix") {

      // requête la base de données pour comptabilisé le nombres d'entrée (table)
      $countRows = $pdo->prepare('SELECT COUNT(*) FROM cards WHERE auteur=:account_name AND categorie=:categorie;');
      $countRows->execute(array(
        ':account_name' => $_SESSION['username'],
        'categorie' => $idMenu
      ));
      $countRows->setFetchMode(PDO::FETCH_OBJ);
      $count = $countRows->fetchAll();
      $_SESSION['countRows'] = $count[0]->{'COUNT(*)'};

    }
  }

  // Retourne true si le menu sélectionné correspond à $keyToCheck
  // On utilise une fonction anonyme ici, de façon a pouvoir capturer une variable extérieure ($idMenu)
  $menuSelected = function($keyToCheck) use ($idMenu) {
    echo ($idMenu === $keyToCheck ? 'selected' : '');
  };

  /*--------------------- PAGINATION PAGES CREATE ---------------------------*/

  $limite = 3;
  // Tableau de tous les cas de figure de la nav
  $classArr = array(
    'classActive' => 'class="page-item active" aria-current="page"',
    'spanActive' => '<span class="sr-only">(current)</span>',
    'class' => 'class="page-item"',
    'span' =>''
  );
  $prev = array(
    'tabindex' =>'tabindex="-1" aria-disabled="true"',
    'disabled' => 'class="page-item disabled"',
    'tabindexActive' =>'',
    'disabledActive' => 'class="page-item"'
  );
  $next = array(
    'tabindex' =>'tabindex="-1" aria-disabled="true"',
    'disabled' => 'class="page-item disabled"',
    'tabindexActive' =>'',
    'disabledActive' => 'class="page-item"'
  );

  // Trouve le nombre de lignes sql en session pour déterminer le nombre d'url de pages
  $countSQL = $_SESSION['countRows'];

  if (($countSQL % $limite) == 0) {
    $countPage = (int)($_SESSION['countRows'] / $limite);
  } else {
    $countPage = (int)($_SESSION['countRows'] / $limite) + 1;
  }
  // Résultats de quantité de page
  $page = array();
  for ($k = 0; $k < $countPage; $k++) {
      array_push($page, ($k + 1));
  }

  // Retrouve le numéro de page dans l'url de la page web
  $current = (!empty($_GET['page']) ? $_GET['page'] : 1);
  $currentMax = $countPage;

  // Récupére des lignes de table à afficher dans un ARRAY d'OBJET
  $debut = ($current - 1) * $limite;

  $req4 = $pdo->prepare('SELECT id, auteur, titre, contenu, dateAjout, dateModif, urlsID, urlName, categorie, imgNom, imgType, imgTaille FROM cards ORDER BY dateModif DESC LIMIT :limite OFFSET :debut;');
  $req4->bindValue('limite', $limite, PDO::PARAM_INT);
  $req4->bindValue('debut', $debut, PDO::PARAM_INT);
  $req4->execute();
  $req4->setFetchMode(PDO::FETCH_OBJ);
  $newsCreate = $req4->fetchAll();

  $idMenu = "choix";
  if (isset($_POST['idMenu'])) {
    $idMenu = htmlspecialchars($_POST['idMenu']);
    // var_dump($_POST['idMenu']);
    if ($idMenu != "choix") {

      $req5 = $pdo->prepare('SELECT id, auteur, titre, contenu, dateAjout, dateModif, urlsID, urlName, categorie, imgNom, imgType, imgTaille FROM cards WHERE auteur=:account_name AND categorie=:categorie ORDER BY dateModif DESC;');
      $req5->execute(array(
        ':account_name' => $_SESSION['username'],
        'categorie' => $idMenu
      ));
      $req5->setFetchMode(PDO::FETCH_OBJ);
      $newsCreate = $req5->fetchAll();

    }
  }

  // Tableau des catégories du menu déroulant
  $arrCat = [];
  foreach ($newsCreate as $key => $value) {
    $res = $newsCreate[$key]->{'categorie'};
    if (array_search($res, $arrCat) === false) {
      $arrCat[] = $res;
    }
  }

  // Détermine un tableau des classes actives pour l'affichage des chiffres
  // en fonction de l'url de page : $current
  if ($current > $limite) {
    $max = $current - 2;
    $temp = $current;
  } else {
    $max = 1;
    $temp = $currentMax;
  }
  for ($n = $max; $n <= $temp; $n++) {
    $pageIndex = $n;
    if ($pageIndex == $current) {
      $class = $classArr['classActive'];
      $span = $classArr['spanActive'];
    } else {
      $class = $classArr['class'];
      $span = $classArr['span'];
    }
    $arrCr [] = (object) [
      'pageIndex' => $pageIndex,
      'class' => $class,
      'span' => $span
    ];
  }

  // Détermine un tableau des classes actives pour l'affichage des PREV et NEXT
  // en fonction de l'url de page : $current
  for ($m = 0; $m < $currentMax; $m++) {
    if ($page[$m] < 4) {
      if ($current < 2) {
        $tabindexPrev = $prev['tabindex'];
        $disabledPrev = $prev['disabled'];
        $tabindexNext = $next['tabindex'];
        $disabledNext = $next['disabled'];
      } else {
        $tabindexPrev = $prev['tabindexActive'];
        $disabledPrev = $prev['disabledActive'];
        $tabindexNext = $next['tabindex'];
        $disabledNext = $next['disabled'];
      }
    }
    if ($page[$m] > 1) {
      if ($current < $currentMax) {
        $tabindexPrev = $prev['tabindex'];
        $disabledPrev = $prev['disabled'];
        $tabindexNext = $next['tabindexActive'];
        $disabledNext = $next['disabledActive'];
      } else if ($current = $currentMax) {
        $tabindexPrev = $prev['tabindexActive'];
        $disabledPrev = $prev['disabledActive'];
        $tabindexNext = $next['tabindex'];
        $disabledNext = $next['disabled'];
      }
    }
  }

// }

// // Obtient les données du sites pour les eventID de l'urlsID
// $getPortalsSQL = new Portals($token);
// $req2 = $getPortalsSQL->getPortalsSQL($token);
// if ($req2 !== '[]' && $req2 !== false) {
//   $resEvent = json_decode($req2)->{'data'};
//   // var_dump($resEvent);
// }




// require_once '../home/footer.php';

?>