<?php

// require_once '../home/header.php';
require_once '../class/portals.php';
require_once '../class/urlssites.php';
// require_once 'pdo.php';
// require_once 'utiles.php';


// --------------------------ONGLET DASHBOARD

// if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == $path . '/home/dashboard.php') {

  if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == $path . '/home/dashboard.php' && isset($_POST['capture']) && isset($_POST['dateCapt']) && !empty($_POST['urlCapt'])) {
    // Récupération des élèments du formulaire pour l'action de l'utilisateur sur le bouton "capture"
    $urlCapt = '';
    $_SESSION['urlCapt'] = '';
    $_SESSION['urlCapt'] = htmlspecialchars($_POST['urlCapt']);
    unset($_POST['urlCapt']);
    $urlCapt = $_SESSION['urlCapt'];
    $dateCapt = htmlspecialchars($_POST['dateCapt']);
    $siteID = $_SESSION['siteID'];

    // Tableau qui prépare les données à la création de la nouvelle UrlsID
    $endDate = new DateTime('2019-10-20');
    $newendDate = $endDate->format('Y-m-d H:i:s');
    $newData = array(
      'urlsCreated_at' => date('Y-m-d H:i:s'),
      'urls' => array(
        'url' => $_SESSION['urlCapt'],
        'urlCreated_at' => date('Y-m-d H:i:s'),
        'beginDate' => date('Y-m-d H:i:s', strtotime($dateCapt)),
        'endDate' => $newendDate,
        'interval' => null,
        'totEv' => null,
        'urlStatus' => 0
      ),
      'sumUrls' => 0,
      'sumUrlsEv' => 0,
      'siteIDFK' =>  $_SESSION['siteID']
    );

    // Recherche si l'Url fournit pas l'utilisateur existe déjà -> pas d'information à l'utilisateur si false
    $exist = urlCaptExist($urlCapt);
    if ($exist == false) {
      // Créer un nouveau UrlsID avec les données de l'utilisateur
      $createUrlsSites = new UrlsSites($token);
      $createUrlsSites->createUrlsSiteSQL($newData, $token);
      $urlCapt = '';
      $_SESSION['urlCapt'] = $urlCapt;
    }

    // début------A SUPPRIMER
    // // Récupération de $urlsID
    // $getBySiteID = new Urlssites();
    // $req3 = $getBySiteID->getLastUrlsID();
    // $_SESSION['urlsID'] = json_decode($req3)->{'urlsIDmax'};

    // $dateCapt = $_POST['dateCapt'];
    // $urlsID = $_SESSION['urlsID'];
    // var_dump($_SESSION['urlsID']);

    // var_dump($setdataUrl);
    // updateUrlsSite($urlsID, $setdataUrl);
    // fin------A SUPPRIMER
  }

  //début----------------A SUPPRIMER
  // function urlSiteExitSQL($urlsID) {
  //   $getUrlSite = new UrlsSites();
  //   // $req = $getUrlSite->getUrlsIDSQLById($urlsID);
  //   $res = json_decode($req);
  //   if (isset($res->{'urls'})) {
  //     return true;
  //   }
  //   return false;
  // }
  //fin-----------------A SUPPRIMER

  if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == $path . '/home/dashboard.php' && isset($_POST['on']) && $_POST['on'] == 'on' && isset($_POST['intervalEv']) && isset($_POST['urlsPost'])) {
    // Récupération des élèments du formulaire pour l'action de l'utilisateur sur le bouton "on"
    if (htmlspecialchars($_POST['on']) !== 'on') {
      $_SESSION['urlsIDOn'] = '';
      unset($_POST['urlsPost']);
    } else {
      $_SESSION['urlsIDOn'] = $_POST['urlsPost'];
      unset($_POST['urlsPost']);
    };
    $urlsIDOn = $_SESSION['urlsIDOn'];
    $intervalEv = htmlspecialchars($_POST['intervalEv']);
    $urlName = htmlspecialchars($_POST['urlName']);

    // Création d'un Timestamp pour crée ultrieurement une nvouvelle url -> attention obsolète pour le dev
    $timestamp = strtotime('now');
    $newUrl = '?date=' . $timestamp;

    // Obtient les données par l'UrlsID posté par l'utilisateur via le bouton "on"
    $getUrlsSitesOn = new UrlsSites($token);
    $reqGetOn = $getUrlsSitesOn->getUrlsIDSQLById($urlsIDOn, $token);

    // Traitement des informations contenues avec l'UrlsID de son utilisateur
    if ($reqGetOn !== '{}') {
      $resGetOn = json_decode(json_decode($reqGetOn)->{'urls'});
      $dateBegin = $resGetOn->{'beginDate'};
      $totEv = $resGetOn->{'totEv'};
      $urlStatus = (date('Y-m-d', strtotime($dateBegin)) == date('Y-m-d')) ? 1 : 0;

      // Convertit les données de l'interval en min(entier);
      $hours = substr($intervalEv, 0, 2);
      $minutes = substr($intervalEv, 3) + $hours * 60;
      $interval = $minutes;

      // Crée un tableau avec les données à renvoyer à l'UrlsID de son l'utilisateur
      $updatedataURLOn = array(
        'interval' => $interval,
        'totEv' => $totEv +1,
        'urlStatus' => $urlStatus
      );

      // Met à jour les informations de l'UrlsID de son utilisateur avec celles existantes non modifiés
      $updateUrlOn = new UrlsSites($token);
      $updateUrlOn->updateUrlSQLById($urlsIDOn, $updatedataURLOn, $token);

      //début-----------------------A SUPPRIMER
      // $timeZone = new DateTimeZone('Europe/Paris');
      // $newdateBegin = new DateTime();
      // $newdateBegin->setTimezone($timeZone);
      // $newdateBegin->format('Y-m-d H:i:s')

      // $hours = substr($intervalEv, 0, 2);
      // $minutes = substr($intervalEv, 3) + $hours * 60;
      // $interval = $minutes . ' minutes';
      // var_dump($interval);
      //fin---------------------- A SUPPRIMER

      // Conditionne la création d'un portail de l'UrlsID de l'utilisateur
      $newdateOn = (new DateTime())->format('Y-m-d H:i:s'); 
      $newdateInterval = new DateTime();
      $newdateInterval->add(new DateInterval('PT' . $minutes . 'M'));
      $newdateOff = $newdateInterval->format('Y-m-d H:i:s');

      // Crée un tableau avec les données à renvoyer de l'utilisateur
      $newEv = array(
        'eventCreated_at' => date('Y-m-d H:i:s'),
        'urlName' => $urlName,
        'intervalEv' => $interval,
        'newUrl' => $newUrl,
        'beginEv' => $newdateOn,
        'endEv' => $newdateOff,
        'statusEv' => 0,
        'urlsIDFK' => $urlsIDOn,
        'dataJson' => null
      );

      // // Recherche si un Portail existe et return true;
      // $getEvUrlsIDTot = new Portals($token);
      // var_dump($urlsIDOn)
      // $reqOn = $getEvUrlsIDTot->getPortalsSQLByUrlsId($urlsIDOn, $token);
      // var_dump($reqOn);
      // $result = true;
      // if ($reqOn !== '[]' && $reqOn !== false) {
      //   $done = json_decode($reqOn)->{'data'};
      //   $result = false;
      //   foreach ($done as $key => $value) {
      //     if ($done[$key]->{'urlsIDFK'} == $urlsIDOn) {
      //       $result = true;
      //     }
      //   }
      // }
      // Crée un portail s'il n'existe pas avec les données précédement préparés
      // if ($result) {
        $createPortals = new Portals($token);
        $createPortals->createPortalsSQL($newEv, $token);
      // }
    }

    //début-----------------------A SUPPRIMER
    // var_dump($newEv['beginEv']);
    // var_dump($newEv['statusEv']);
    // var_dump($newEv['endEv']);

    // if ($statusEv) {
    //   if ($beginEv > $today) {
    //     $createPortals = new Portals();
    //     $createPortals->createPortalsSQL($newEv);
    //   }
    // }
    
    // $dateBegin = '2019-10-04 04:00';
    // $dateBegin = $newEv['beginEv'];
    // $interval = $newEv['intervalEv'];
    // $dateEnd = '2019-10-10 18:10';

    // timeEvent($createPortals, $intervalEv);
    // function timeEvent($createPortals, $intervalEv) {
      
    // }
    //fin-----------------------A SUPPRIMER

    unset($_POST['off']);
  }

  //début-----------------------A SUPPRIMER
  // $intervalEv = 62;
  // $interval = (isset($intervalEv) == true) ? octal(intdiv($intervalEv, 60)) . ':' . octal($intervalEv % 60) : '00:00';
  // var_dump($interval);
  //fin-----------------------A SUPPRIMER

  if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == $path . '/home/dashboard.php' && isset($_POST['off']) && $_POST['off'] == 'off' && isset($_POST['intervalEv']) && isset($_POST['urlsPost'])) {
    // Récupération des élèments du formulaire pour l'action de l'utilisateur sur le bouton "off"
    if (htmlspecialchars($_POST['off']) !== 'off') {
      $_SESSION['urlsIDOff'] = '';
      unset($_POST['urlsPost']);
    } else {
      $_SESSION['urlsIDOff'] = $_POST['urlsPost'];
      unset($_POST['urlsPost']);
    };
    $urlsIDOff = $_SESSION['urlsIDOff'];
    $intervalEv = htmlspecialchars($_POST['intervalEv']);

    // Convertit les données de l'interval en min(entier);
    $hours = substr($intervalEv, 0, 2);
    $minutes = substr($intervalEv, 3) + $hours * 60;
    $interval = $minutes;

    // Récupère les données de l'UrlsID de l'utilisateur
    $getUrlsSitesOff = new UrlsSites($token);
    $reqGetOff = $getUrlsSitesOff->getUrlsIDSQLById($urlsIDOff, $token);
    if ($reqGetOff !== '{}' && $reqGetOff !== false) {
      $done = json_decode($reqGetOff)->{'urls'};
      $totEvOff = json_decode($done)->{'totEv'};
    }

    // Crée un tableau avec les données à renvoyer à l'UrlsID de son l'utilisateur
    $updatedataURLOff = array(
      'interval' => $interval,
      'totEv' => $totEvOff,
      'urlStatus' => 0
    );

    // Met à jour les informations de l'UrlsID de son utilisateur avec celles existantes non modifiés
    $updateUrlOff = new UrlsSites($token);
    $updateUrlOff->updateUrlSQLById($urlsIDOff, $updatedataURLOff, $token);

    unset($_POST['on']);
  }

  if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == $path . '/home/dashboard.php' && !empty($_POST['deleteUrl']) && isset($_POST['deleteUrl']) && isset($_POST['urlsPost'])) {

    // Récupére des élèments du formulaire pour l'action de l'utilisateur sur le bouton "X" de l'UrlsID
    if (!isset($_POST['deleteUrl'])) { 
      $_SESSION['urlsID'] = '';
      unset($_POST['urlsPost']); 
    } else {
      $_SESSION['urlsID'] = htmlspecialchars($_POST['urlsPost']);
      unset($_POST['urlsPost']);
    };
    $urlsID = $_SESSION['urlsID'];

    if (!empty($urlsID)) {
      // var_dump((int)$urlID);
      $getEvUrlsID = new Portals($token);
      $req = $getEvUrlsID->getPortalsSQLByUrlsId($urlsID, $token);
      if ($req !== '[]' && $req !== false) {
        $reqDelete = json_decode($req)->{'data'};
        $res = false;
        // var_dump($reqDelete);
        foreach ($reqDelete as $key => $value) {
          if ($reqDelete[$key]->{'urlsIDFK'} == $urlsID) {
            $eventID = $reqDelete[$key]->{'eventID'};
            // var_dump((int)$eventID);
            $deleteEvUrls = new Portals($token);
            $deleteEvUrls->deleteEventSQL((int)$eventID, $token);
            $res = true;
          } else if ($reqDelete[$key]->{'urlsIDFK'} == null) {
            $res = false;
          }
        }
        if ($res == true) {
          $deleteUrl = new UrlsSites($token);
          $deleteUrl->deleteUrlSQL((int)$urlsID, $token);
        }
      } else {
        $deleteUrl = new UrlsSites($token);
        $deleteUrl->deleteUrlSQL((int)$urlsID, $token);
      }
    }
  }

  if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == $path . '/home/dashboard.php' && !empty($_POST['deleteEvent']) && isset($_POST['deleteEvent']) && isset($_POST['eventID'])) {
    // Récupére des élèments du formulaire pour l'action de l'utilisateur sur le bouton "X" de l'eventID
    if (isset($_POST['deleteUrl'])) { 
      $_SESSION['eventID'] = '';
      unset($_POST['eventID']); 
    } else {
      $_SESSION['eventID'] = htmlspecialchars($_POST['eventID']);
      unset($_POST['eventID']);
    };
    $eventID = $_SESSION['eventID'];

  // Tableau des données à mettre à jour pour l'eventID -> attention delete possible par le dev
    if (!empty($eventID)) {
      // var_dump((int)$eventID);
      // $deleteEvent = new Portals();
      // $deleteEvent->deleteEventSQL((int)$eventID);
      $upDataEvDown = array(
        'statusEv' => 0
      );
      if (isset($eventID)) {
        $downPortals = new Portals($token);
        $downPortals->updatePortalsSQLById((int)$eventID, $upDataEvDown, $token);
      }
    }
  }
  //début----------------A SUPRIMER
  // if (isset($_POST['afficher']) && !empty($_POST['afficher']) && isset($_POST['eventID'])) {
  //   $eventID = $_POST['eventID'];
  //   $afficherUrl = new Portals();
  //   // $vueApi = $afficherUrl->afficherEvent((int)$eventID);
  //   echo $vueApi;
  // }

  // $countEvents = new Portals();
  // $totalEv = $countEvents->countEvents($siteID);
  //fin----------------A SUPRIMER


    // requête toute la base de donnée pour les articles de son utilisateur
    $req = $pdo->prepare('SELECT id, auteur, titre, contenu, dateAjout, dateModif, urlsID, urlName, categorie, imgNom, imgType, imgTaille FROM cards WHERE auteur=:account_name ORDER BY dateModif DESC;');
    $req->execute(array(':account_name' => $_SESSION['username']));
    $req->setFetchMode(PDO::FETCH_OBJ);
    $newsPerso = $req->fetchAll();
    // var_dump($newsPerso);

    $arrPerso = [];
    foreach ($newsPerso as $key => $value) {
      $resUrlName = $newsPerso[$key]->{'urlName'};
      $resCat = $newsPerso[$key]->{'categorie'};
      array_push($arrPerso, (object)array(
        'urlName' => $resUrlName,
        'categorie' => $resCat
      ));
    }
    // var_dump($arrPerso);

    $idMenu = "choix";
    if (isset($_POST['idMenu'])) {
      $idMenu = htmlspecialchars($_POST['idMenu']);
      // var_dump($_POST['idMenu']);
      if ($idMenu != "choix") {

        $req = $pdo->prepare('SELECT id, auteur, titre, contenu, dateAjout, dateModif, urlsID, urlName, categorie, imgNom, imgType, imgTaille FROM cards WHERE auteur=:account_name AND categorie=:categorie ORDER BY dateModif DESC;');
        $req->execute(array(
          ':account_name' => $_SESSION['username'],
          'categorie' => $idMenu
        ));
        $req->setFetchMode(PDO::FETCH_OBJ);
        $newsPerso = $req->fetchAll();

        foreach ($newsPerso as $key => $value) {
          $resUrlName = $newsPerso[$key]->{'urlName'};
          $resCat = $newsPerso[$key]->{'categorie'};
          array_push($arrPerso, (object)array(
            'urlName' => $resUrlName,
            'categorie' => $resCat
          ));
        }
        // var_dump($arrPerso);

      }
    }

  // Retourne true si le menu sélectionné correspond à $keyToCheck
  // On utilise une fonction anonyme ici, de façon a pouvoir capturer une variable extérieure ($idMenu)
  $menuSelected = function($keyToCheck) use ($idMenu) {
    echo ($idMenu === $keyToCheck ? 'selected' : '');
  };


  // Obtient les données du sites de l'urlsID-> attention where $_SESSION['siteID']
  $getUrlsSQL = new UrlsSites($token);
  $req1 = $getUrlsSQL->getUrlsIDSQL($token);
  if ($req1 != '{"message":"No token provided."}' && $req1 !== '[]' && $req1 !== false) {
    $resCapt = json_decode($req1)->{'data'};
  }

  // Obtient les données du sites pour les eventID de l'urlsID
  $getPortalsSQL = new Portals($token);
  $req2 = $getPortalsSQL->getPortalsSQL($token);
  if ($req2 != '{"message":"No token provided."}' && $req2 != '[]' && $req2 != false) {
    $resEvent = json_decode($req2)->{'data'};
  }

  $arrCount = [];
  if (isset($resEvent)) {
    foreach ($resEvent as $key => $value) {
      $resEvUrl = $resEvent[$key]->{'urlName'};
      if (array_search($resEvUrl, $arrCount) === false) {
        array_push($arrCount, $resEvUrl);
      }
    }
  }
  // var_dump($arrCount);
  $_SESSION['countEv'] =  count($arrCount);

  /* ---------------------PAGINATION DASHBOARD ---------------------------*/
  $limite = 3;
  // Tableau de tous les cas de figure de la nav
  $classArr = array(
    'classActive' => 'class="page-item active" aria-current="page"',
    'spanActive' => '<span class="sr-only">(current)</span>',
    'class' => 'class="page-item"',
    'span' =>''
  );
  $prev = array(
    'tabindex' =>'tabindex="-1" aria-disabled="true"',
    'disabled' => 'class="page-item disabled"',
    'tabindexActive' =>'',
    'disabledActive' => 'class="page-item"'
  );
  $next = array(
    'tabindex' =>'tabindex="-1" aria-disabled="true"',
    'disabled' => 'class="page-item disabled"',
    'tabindexActive' =>'',
    'disabledActive' => 'class="page-item"'
  );

  // Trouve le nombre de lignes sql en session pour déterminer le nombre d'url de pages
  $countSQL = $_SESSION['countEv'];

  if ($countSQL > 0) {
    if (($countSQL % $limite) == 0) {
      $countPage = (int)($_SESSION['countEv'] / $limite);
    } else {
      $countPage = (int)($_SESSION['countEv'] / $limite) + 1;
    }
  } else {
    $countPage = 1;
  }
  // Résultats de quantité de page
  $page = array();
  for ($k = 0; $k < $countPage; $k++) {
      array_push($page, ($k + 1));
  }

  // Retrouve le numéro de page dans l'url de la page web
  $current = (!empty($_GET['page']) ? $_GET['page'] : 1);
  $currentMax = $countPage;

  // Récupére des lignes de table à afficher dans un ARRAY d'OBJET

  $debut = ($current - 1) * $limite;

  $arrUrlCat = [];
  $resArr = [];
  if (isset($resEvent)) {
    foreach ($resEvent as $key => $value) {
      $resEvUrl = $resEvent[$key]->{'urlName'};
      foreach ($arrPerso as $key => $value) {
        $res = $arrPerso[$key]->{'urlName'};
        if ($res = $resEvUrl) {
          if (array_search($res, $arrUrlCat) === false) {
            array_push($arrUrlCat, $res);
            array_push($resArr, (object)array(
              'urlName' => $arrPerso[$key]->{'urlName'},
              'categorie' => $arrPerso[$key]->{'categorie'}
            ));
          }
        }
      }
    }
    // var_dump($resArr);
  }

  // $arrEvCat = [];
  // foreach ($arrPerso as $key => $value) {
  //   $res = $arrPerso[$key]->{'categorie'};
  //   if (array_search($res, $arrEvCat) === false) {
  //     array_push($arrEvCat, $res);
  //   }
  // }
  // var_dump($arrEvCat);

  $newsPage = [];
  // var_dump($resEvent);
  if (isset($resEvent)) {
    foreach ($resEvent as $key => $value) {
      isset($resEvent[$key]->{'dataJson'}) ? $resEvent[$key]->{'dataJson'} : '';
      if (isset($resEvent[$key]->{'dataJson'})) {
        // if ($key >= $debut) {
        //   if ($key < $limite + $debut) {
            $resEvID = $resEvent[$key]->{'eventID'};
            $resEvCreated = $resEvent[$key]->{'eventCreated_at'};
            $resEvUrl = $resEvent[$key]->{'urlName'};
            $resEvInt = $resEvent[$key]->{'intervalEv'};
            $resEvNewUrl = $resEvent[$key]->{'newUrl'};
            $resEvBegin = $resEvent[$key]->{'beginEv'};
            $resEvEnd = $resEvent[$key]->{'endEv'};
            $resEvStatus = $resEvent[$key]->{'statusEv'};
            $resEvUrlsID = $resEvent[$key]->{'urlsIDFK'};
            // $resEvData = (isset($resEvent[$key]->{'dataJson'})) ? $resEvent[$key]->{'dataJson'} : '';
            $resEvData = $resEvent[$key]->{'dataJson'};

            $arrEv = array(
              'eventID' => $resEvID,
              'eventCreated_at' => $resEvCreated,
              'urlName' => $resEvUrl,
              'intervalEv' => $resEvInt,
              'newUrl' => $resEvNewUrl,
              'beginEv' => $resEvBegin,
              'endEv' => $resEvEnd,
              'statusEv' => $resEvStatus,
              'urlsIDFK' => $resEvStatus,
              'dataJson' => $resEvData
            );
            array_push($newsPage, $arrEv);
          }
      //   }
      // }
    }
  }
  $newsEv = json_decode(json_encode($newsPage), FALSE);
  krsort($newsEv);
  // var_dump($newsEv);
  // var_dump($resArr);

  $idMenu = "choix";
  if (isset($_POST['idMenu'])) {
    $idMenu = htmlspecialchars($_POST['idMenu']);
    // var_dump($_POST['idMenu']);
    if ($idMenu != "choix") {

      foreach ($resArr as $key => $value) {
        $resCat = $resArr[$key]->{'categorie'};
        $resUrl = $resArr[$key]->{'urlName'};
        if ($idMenu == $resCat) {
          foreach ($resEvent as $key => $value) {
            isset($resEvent[$key]->{'dataJson'}) ? $resEvent[$key]->{'dataJson'} : '';
            if (isset($resEvent[$key]->{'dataJson'})) {
              $resEvUrl = $resEvent[$key]->{'urlName'};
              if ($resEvUrl == $resUrl) {

                array_push($reqArr, $resEvUrl);
                if (array_search($resEvUrl, $reqArr) === false) {
                  if ($resUrl = $resEvUrl) {
                    if ($key >= $debut) {
                      if ($key < $limite + $debut) {
                        $resEvID = $resEvent[$key]->{'eventID'};
                        $resEvCreated = $resEvent[$key]->{'eventCreated_at'};
                        $resEvUrl = $resEvent[$key]->{'urlName'};
                        $resEvInt = $resEvent[$key]->{'intervalEv'};
                        $resEvNewUrl = $resEvent[$key]->{'newUrl'};
                        $resEvBegin = $resEvent[$key]->{'beginEv'};
                        $resEvEnd = $resEvent[$key]->{'endEv'};
                        $resEvStatus = $resEvent[$key]->{'statusEv'};
                        $resEvUrlsID = $resEvent[$key]->{'urlsIDFK'};
                        // $resEvData = (isset($resEvent[$key]->{'dataJson'})) ? $resEvent[$key]->{'dataJson'} : '';
                        $resEvData = $resEvent[$key]->{'dataJson'};

                        $arrEv = array(
                          'eventID' => $resEvID,
                          'eventCreated_at' => $resEvCreated,
                          'urlName' => $resEvUrl,
                          'intervalEv' => $resEvInt,
                          'newUrl' => $resEvNewUrl,
                          'beginEv' => $resEvBegin,
                          'endEv' => $resEvEnd,
                          'statusEv' => $resEvStatus,
                          'urlsIDFK' => $resEvStatus,
                          'dataJson' => $resEvData
                        );
                        array_push($newsPage, $arrEv);
                      }
                    }
                  }
                }

              }
            }
          }
        }
      }
      $newsEv = json_decode(json_encode($newsPage), FALSE);
      krsort($newsEv);
      // var_dump($newsEv);
    }
  }

/* --------------------------*/

  // Détermine un tableau des classes actives pour l'affichage des chiffres
  // en fonction de l'url de page : $current
  if ($current > $limite) {
    $max = $current - 2;
    $temp = $current;
  } else {
    $max = 1;
    $temp = $currentMax;
  }
  for ($n = $max; $n <= $temp; $n++) {
    $pageIndex = $n;
    if ($pageIndex == $current) {
      $class = $classArr['classActive'];
      $span = $classArr['spanActive'];
    } else {
      $class = $classArr['class'];
      $span = $classArr['span'];
    }
    $arrDh[] = (object) [
      'pageIndex' => $pageIndex,
      'class' => $class,
      'span' => $span
    ];
    // var_dump($arrDh);
  }
  // var_dump($arrDh);

  // Détermine un tableau des classes actives pour l'affichage des PREV et NEXT
  // en fonction de l'url de page : $current
  for ($m = 0; $m < $currentMax; $m++) {
    if ($page[$m] < 4) {
      if ($current < 2) {
        $tabindexPrev = $prev['tabindex'];
        $disabledPrev = $prev['disabled'];
        $tabindexNext = $next['tabindex'];
        $disabledNext = $next['disabled'];
      } else {
        $tabindexPrev = $prev['tabindexActive'];
        $disabledPrev = $prev['disabledActive'];
        $tabindexNext = $next['tabindex'];
        $disabledNext = $next['disabled'];
      }
    }
    if ($page[$m] > 1) {
      if ($current < $currentMax) {
        $tabindexPrev = $prev['tabindex'];
        $disabledPrev = $prev['disabled'];
        $tabindexNext = $next['tabindexActive'];
        $disabledNext = $next['disabledActive'];
      } else if ($current = $currentMax) {
        $tabindexPrev = $prev['tabindexActive'];
        $disabledPrev = $prev['disabledActive'];
        $tabindexNext = $next['tabindex'];
        $disabledNext = $next['disabled'];
      }
    }
  }

/* -------------------PAGE PROFIL------------------------- */

// if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == $path . '/home/profil.php') {

//   $username = $_SESSION['username'];
//   // Obtient les informations du profil de l'utilisateur */
//   $res = $loginCookie->get_account($username, $db);
//   var_dump($res);
//   // $loginCookie->enabled_account($account_id, $db);
  
//   $account_enabled = ($res['account_enabled'] == '1') ? 'actif' : 'inactif';
//   $account_name = $username;
//   $account_email = $res['account_email'];
//   $confirmed_at = $res['confirmed_at'];
//   $confirmation_token = $res['confirmation_token'];
//   $account_confirmed = date('Y-m-d H:i:s');
//   $confirmed_at = $account_confirmed;

// }
/* ----------------------------------------- */

// require_once '../home/footer.php';

?>