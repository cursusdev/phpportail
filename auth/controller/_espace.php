<?php

// require_once '../home/header.php';
require_once 'pdo.php';
require_once '../class/portals.php';
// require_once 'utiles.php';


// --------------------------ONGLET MON ESPACE

if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == $path . '/home/espace.php') {

  // requête toute la base de donnée pour les articles de son utilisateur
  $req = $pdo->prepare('SELECT id, auteur, titre, contenu, dateAjout, dateModif, urlsID, urlName, categorie, imgNom, imgType, imgTaille FROM cards WHERE auteur=:account_name ORDER BY dateModif DESC;');
  $req->execute(array(':account_name' =>  $_SESSION['username']));
  $req->setFetchMode(PDO::FETCH_OBJ);
  $newsPerso = $req->fetchAll();
  // var_dump($newsPerso);

  $arrCat = [];
  foreach ($newsPerso as $key => $value) {
    $res = $newsPerso[$key]->{'categorie'};
    if (array_search($res, $arrCat) === false) {
      $arrCat[] = $res;
    }
  }

  $idMenu = "choix";
  if (isset($_POST['idMenu'])) {
    $idMenu = htmlspecialchars($_POST['idMenu']);
    if ($idMenu != "choix") {
      $req = $pdo->prepare('SELECT id, auteur, titre, contenu, dateAjout, dateModif, urlsID, urlName, categorie, imgNom, imgType, imgTaille FROM cards WHERE auteur=:account_name AND categorie=:categorie ORDER BY dateModif DESC;');
      $req->execute(array(
        ':account_name' => $_SESSION['username'],
        'categorie' => $idMenu
      ));
      $req->setFetchMode(PDO::FETCH_OBJ);
      $newsPerso = $req->fetchAll();
    }
  }

  // Retourne true si le menu sélectionné correspond à $keyToCheck
  // On utilise une fonction anonyme ici, de façon a pouvoir capturer une variable extérieure ($idMenu)
  $menuSelected = function($keyToCheck) use ($idMenu) {
    echo ($idMenu === $keyToCheck ? 'selected' : '');
  };

  //   $url = 'http://api.open-notify.org/astros.json';
  //   if (isset($newsPerso) && !empty($newsPerso)) {
  //     if (isset($resEvent)) {
  //       // urlNameBadge($url);
  //     }
  //   }

  // // Obtient les données du sites pour les eventID de l'urlsID
  // // $getPortalsSQL = new Portals($token);
  // $req2 = $getPortalsSQL->getPortalsSQL($token);
  // if ($req2 !== '[]' && $req2 !== false) {
  //   $resEvent = json_decode($req2)->{'data'};
  //   // var_dump($resEvent);
  // }


  // function urlNameBadge($resEvent, $url) {
  //   // var_dump($resEvent);
  //   foreach ($resEvent as $key => $value) {
  //     if ($resEvent[$key]->{'statusEv'} === true) {
  //       $urlName = $resEvent[$key]->{'urlName'};
  //       if ($urlName == $url) {
  //         $beginEv = $resEvent[$key]->{'beginEv'};
  //         $intervalEv = $resEvent[$key]->{'intervalEv'};
  //         // $endEv = date('d/m/Y', $beginEv);
  //         // $endTime = date('H:i:s', $beginEv);
  //         $endEv = $beginEv;
  //         $endTime = $beginEv;
  //         $eventID = $resEvent[$key]->{'eventID'};

  //         $datetime = new DateTime($beginEv);
  //         // echo $datetime->format('Y');
  //         $sem = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
  //         $jSem = $datetime->format('w');
  //         $dateEv = $datetime->format('d/m/Y');
  //         $timeEv = $datetime->format('H:m:s');
  //       }
  //     }
  //   }
  // }

// }

}

// require_once '../home/footer.php';

?>