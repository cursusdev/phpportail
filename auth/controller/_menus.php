<?php

// require_once '../home/header.php';
// require_once 'pdo.php';
// require_once 'utiles.php';


// --------------------------MENUS

if(isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == $path . '/home/menu.php' && isset($_SESSION['role']) && $_SESSION['role'] == 'visiteur') {
  // Si visiteur
  $classAdmin = 'style="display:none;"';
  $hrefAdmin = '#"';
} else if(isset($_SESSION['role']) && $_SESSION['role'] == 'admin') {
  // Si administrateur
  $classAdmin = 'class="nav-link" style="color:blue;"';
} else {
  // utilisateur
  $classUser = 'class="nav-link"';
  $hrefDashboard = 'href="' . $path . '/home/dashboard.php"';
  $hrefPerso = 'href="' . $path . '/home/espace.php"';
  $hrefCreate = 'href="' . $path . '/home/create.php"';
  $hrefProfil = 'href="' . $path . '/home/profil.php"';
}

$activeAccueil = ($_SERVER['REQUEST_URI'] == $path . '/home/accueil.php') ? 'active' : '';
// $activeFinancial = ($_SERVER['REQUEST_URI'] == $path . '/home/financial.php') ? 'active' : '';
// $activeUnesco = ($_SERVER['REQUEST_URI'] == $path . '/home/unesco.php') ? 'active' : '';
// $activeIss = ($_SERVER['REQUEST_URI'] == $path . '/home/iss.php') ? 'active' : '';
$activePerso = ($_SERVER['REQUEST_URI'] == $path . '/home/espace.php') ? 'active' : '';
$activeAdmin = ($_SERVER['REQUEST_URI'] == $path . '/home/admin.php') ? 'active' : '';
$activeCreate = ($_SERVER['REQUEST_URI'] == $path . '/home/create.php') ? 'active' : '';
$activeDashboard = ($_SERVER['REQUEST_URI'] == $path . '/home/dashboard.php') ? 'active' : '';
$activeCompte = ($_SERVER['REQUEST_URI'] == $path . '/home/profil.php') ? 'active' : '';


// require_once '../home/footer.php';


?>