<?php

// limite le texte du contenu de l'article - 190 max
function debutTexte($textData, $long) {
    if (strlen ($textData) <= $long) {
        return $textData;
    } else {
        $debut = substr($textData, 0, $long);
        $debut = substr($debut, 0, strrpos($debut, ' ')) . '<span id="points">...</span>';
        return $debut;
    }
}

function urlCaptExist($urlCapt) {
  // Récupère toute les Url contenu dans la base de données pour les comparés -> attention where = $_SESSION['siteID']
  $getUrlsIDSQL = new UrlsSites($token);
  $req = $getUrlsIDSQL->getUrlsIDSQL($token);
  $_SESSION['urlCapt'] = '';
  unset($_POST['urlCapt']);
  $res = false;
  if ($req == '[]') {
    $res = false;
  } else {
    $resCapt = json_decode($req)->{'data'};
    if ($urlCapt == '') {
      $existUrlSite = 'Url doit être renseigné!';
      $res = true;
    } else {
      foreach ($resCapt as $key => $value) {
        // var_dump(json_decode($resCapt[$key]->{'urls'})->{'url'});
        if ($urlCapt == json_decode($resCapt[$key]->{'urls'})->{'url'}) {
          $existUrlSite = 'Url déjà créee!';
          $res = true;
        }
      }
    } 
  }
  return $res;
}

  // Mettre un 0 devant le chiffre
  function octal($nombre) {
    $nombre = (string) $nombre;
    if(strlen($nombre) < 2) {
        $nombre = '0' . $nombre;
    }
    // $nombre = (int) $nombre;
    return $nombre;
  }


?>
