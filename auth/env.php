<?php 

class Env {

  /* Objet PDO à utiliser pour les opérations de base de données phpAuth */
  private static $db;

  /* Objet PDO à utiliser pour les opérations de base de données tpNews */
  private static $pdo;

  /* secret du recaptcha google */
  private static $secretkey;

  private static $host;

  private static $path;
  
  private static $path_auth;

  /* Objet PDO à utiliser pour les opérations de base de données phpAuth */
  private $connection_db;

  /* Objet PDO à utiliser pour les opérations de base de données tpNews */
  private $connection_pdo;

  /* secret du recaptcha google */
  private $data_secretkey;

  private $data_sitekey;

  private $url_host;

  private $url_path;

  private $url_path_auth;


  // Mode Developement
  const DB_USERNAME = 'cursusdev';
  const DB_PASSWORD = 'cursus@2019';
   // endpoint Dev
  const SECRET_CAPTCHA = '6LfmR68UAAAAABBwovzPj8qj2IBBE6Pz986FTT5C';
  const KEY_CAPTCHA = '6LfmR68UAAAAADghOs1ZmgQe7PZmlcASj5UKOv_X';
  const HOST = 'http://127.0.0.1:8080';
  const PATH = '/phpportail';
  const PATH_AUTH = '/phpportail/auth';
  
  // // Mode Production
  // const DB_USERNAME = 'cpses_ug1vbwnefq';
  // const DB_PASSWORD ='cursus@2019';
  // // endpoint Prod
  // const SECRET_CAPTCHA = '6LcMWsQUAAAAAD3XNQvgbL8CFroy2jnukkE6Opg3';
  // const KEY_CAPTCHA = '6LcMWsQUAAAAABggMIWTu-a_MPrfdUcK-E_AfeMo';
  
  // const HOST = 'http://www.somprod.com';
  // const PATH = '/somprod.com/cursusdev';
  // const PATH_AUTH = '/somprod.com/cursusdev/auth';
  
  const DB_HOST = 'localhost';
  const DB_CONNECTION ='mysql';
  const DB_PORT = 3306;
  const DB_DATABASE1 = 'somprodphpauth';
  const DB_DATABASE2 = 'somprodtpnews';
  
  /* Constructeur */
  private function __construct() {
    $this->connection_db = new PDO(self::DB_CONNECTION . ':host=' . self::DB_HOST .':' . self::DB_PORT . ';dbname=' . self::DB_DATABASE1, self::DB_USERNAME, self::DB_PASSWORD);
    $this->connection_pdo = new PDO(self::DB_CONNECTION . ':host=' . self::DB_HOST .':' . self::DB_PORT . ';dbname=' . self::DB_DATABASE2, self::DB_USERNAME, self::DB_PASSWORD);
    $this->data_secretkey = self::SECRET_CAPTCHA;
    $this->data_sitekey = self::KEY_CAPTCHA;
    $this->url_host = self::HOST;
    $this->url_path = self::PATH;
    $this->url_path_auth = self::PATH_AUTH;
  }

  /* Permet la connection à la base de données phpAuth */
  public static function get_db() {
    if (self::$db == null) {
        self::$db = new Env();
    }
    return self::$db->connection_db;
  }

  /* Permet la connection à la base de données tpNews */
  public static function get_pdo() {
    if (self::$pdo == null) {
        self::$pdo = new Env();
    }
    return self::$pdo->connection_pdo;
  }

  /* Permet l'affichage de recaptcha google */
  public static function get_secret() {
    if (self::$secretkey == null) {
        self::$secretkey = new Env();
    }
    return self::$secretkey->data_secretkey;
  }

  public static function get_host() {
    if (self::$host == null) {
        self::$host = new Env();
    }
    return self::$host->url_host;
  }

  public static function get_path() {
    if (self::$path == null) {
        self::$path = new Env();
    }
    return self::$path->url_path;
  }

  public static function get_path_auth() {
    if (self::$path_auth == null) {
        self::$path_auth = new Env();
    }
    return self::$path_auth->url_path_auth;
  }
}
