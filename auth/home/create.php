<?php

// require_once '../vendor/autoload.php';
require_once '../home/header.php';
// require_once '../controller/main.php';


?>
<br />
<style>
.create {
  width: 95%;
}
.formCr {
  padding:0;
}
.form-group input {
  width:90%;
  margin-left:auto;
  margin-right:auto
}
.form-group textarea {
  width:90%;
  margin-left:auto;
  margin-right:auto;
}
.table.liste {
  width: 90%;
  margin-left: 5%;
  text-align: center;
}
.table.liste th {
  border-top: 0 solid white;
  padding: 0 .4rem .4rem .4rem;
  margin: 0;
}
.table.liste td {
  padding: .4rem 0 .4rem 0;
  margin: 0;
}
.titreLs {
  width: 100%;
  text-align: left;
  padding: 0;
  margin: 0 2.5% 0 2.5%;
}
.titreLs span {
  color: blueviolet;
  font-weight: 600;
  margin-left: 2.5%;
}
.titreLs hr {
  width: 95%;
  height: .4rem;
  padding: 0;
  margin: .7rem 0 0 0;
  line-height: 1;
  letter-spacing: 0;
  border-top: 1px solid blueviolet;
}

</style>
<div class="create">

  <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST" name="formulaire" class="formCr" enctype="multipart/form-data">
      <div class="form-group">
          Nom de l'url:<input type="text" class="form-control" name="urlNom" value="<?php if (isset($inputUrl)) { echo $inputUrl; }?>" />
          <?php if (isset($emptyUrl)) { echo $emptyUrl; } ?>
      </div>
      <div class="form-group">
          Titre :<input type="text" class="form-control" name="titre" value="<?php if(isset($inputTitre)) { echo $inputTitre; }?>" />
          <?php if (isset($emptyTitre)) { echo $emptyTitre; } ?>
      </div>
      <div class="form-group">
          <label for="contenu">Contenu :</label>
          <textarea id="contenu" class="form-control" name="contenu" rows="5" cols="33"><?php if (isset($inputContenu)) { echo $inputContenu; }?></textarea>
          <?php if (isset($emptyContenu)) { echo $emptyContenu; } ?>
      </div>
      <div class="form-group">
        Catégorie :<input type="text" class="form-control" name="categorie" value="<?php if(isset($inputCat)) { echo $inputCat;} ?>" />
        <?php if (isset($emptyCat)) { echo $emptyCat; } ?>
      </div>
      <input type="file" lang="fr"  name="file" accept=".jpg,.jpeg,.gif,.png" /><br />
      <?php if (isset($emptyImage)) { echo $emptyImage; } ?>

      <br /><br />
      <input type="submit" class="btn btn-success" name="vider" value="vider" />
      <input type="submit" class="btn btn-primary" name="ajouter" value="Ajouter" />
      <input type="hidden" name="urlsPost" value="<?php if (isset($urlsIDVal)) { echo $urlsIDVal; } ?>" />
      <input type="hidden" name="urlName" value="<?php if (isset($urlName)) { echo $urlName; } ?>" />
  </form>
  <br />
  
  <div class="titreLs">
    <span>Liste<span>
    <hr />
  </div>
  <table class="table liste">
    <tr>
      <th>Image</th>
      <th>Titre</th>
      <th>Date de publication</th>
      <th>Catégorie</th>
      <th>Actions</th>
    </tr>
    <?php
    // var_dump($newsCreate);
    foreach ($newsCreate as $key => $value) {
      $dateAjout = $newsCreate[$key]->{'dateAjout'};
      $newDateAjout = date('d/m/Y', strtotime($dateAjout));
      $dateModif = $newsCreate[$key]->{'dateModif'};
      $newDateModif = date('d/m/y H:i', strtotime($dateModif));
      $newId = $newsCreate[$key]->{'id'};
      $newImg = $newsCreate[$key]->{'imgNom'};
      ?>
    <tr>
      <td><img src="<?php echo $path; ?>/uploads/<?php echo $newImg; ?>" style="width:100px;border:2px solid #ddd"/></td>
      <td><?php echo $newsCreate[$key]->{'titre'}; ?></td>
      <td><?php echo $newDateAjout; ?></td>
      <td><?php echo $newsCreate[$key]->{'categorie'}; ?></td>
      <td>
      <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
        <input type="hidden" name="newId" value="<?php if (isset($newId)) { echo $newId; }?>" />
        <input type="submit" class="btn btn-info" name="editer" value="editer" />
        <input type="submit" class="btn btn-danger" name="x" value="x" />
      </form>
      </td>
    </tr>
    <?php
    }
    ?>
  </table>
  <nav aria-label="nav">
    <ul class="pagination justify-content-center">
      <li <?php echo $disabledPrev; ?>>
        <a class="page-link" href="?page=<?php echo $current -1; ?>" <?php echo $tabindexPrev; ?> >Previous</a>
      </li>
      <?php foreach ($arrCr as $key => $value) { ?>
      <li <?php echo $arrCr[$key]->{'class'}; ?>>
        <a class="page-link" href="?page=<?php echo $arrCr[$key]->{'pageIndex'}; ?>"><?php echo $arrCr[$key]->{'pageIndex'}; ?><?php echo $arrCr[$key]->{'span'}; ?></a>
      </li>
      <?php } ?>
      <li <?php echo $disabledNext; ?>>
        <a class="page-link" href="?page=<?php echo $current +1; ?>" <?php echo $tabindexNext; ?> >Next</a>
      </li>
    </ul>
  </nav>
</div>
<?php


require_once '../home/footer.php';

?>
