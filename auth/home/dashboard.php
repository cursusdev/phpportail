<?php
unset($_POST['urlsID']);

require_once '../home/header.php';
// require_once '../controller/current.php';
// echo 'La valeur id du site est : ' . $_SESSION['siteID'];

?>
<div id="container">
  <!-- <header>
      <h1><?php if (isset($_SESSION['siteName'])) { echo $_SESSION['siteName']; } ?></h1>
  </header>
  <form action="../../index.php" method="post">
      <button class="btn btn-info" name="accueil">Accueil</button>
  </form>
  <br /> -->

  <style>
  input:invalid+span:after {
    content: 'X';
    padding-left: 5px;
  }
  input:valid+span:after {
    content: '✓';
    padding-left: 5px;
  }
  .rating a {
    /* float: right; */
    color: #aaa;
    text-decoration: none;
    font-size: 2em;
    transition: color .6s;
  }
  .rating a:hover,
  .rating a:focus,
  .rating a:hover ~ a,
  .rating a:focus ~ a {
    color: orange;
    /* cursor: pointer; */
  }

  /* ---------------Accordion ------------------*/
  .container {
    width: 100%;
    max-width: 600px;
    margin: 50px auto;
  }
  button.accordion {
    width: 80%;
    background-color: azure;
    border: none;
    outline: none;
    text-align: left;
    padding: 2px 0 0 1rem;;
    font-size: 1.2rem;
    color: #444;
    /* cursor: pointer; */
    transition: background-color 0.2s linear;
    border-top-right-radius: 10px;
    border-top-left-radius: 10px;
    word-wrap: break-word;
  }
  button.accordion:after {
    /* content: '\f055'; */
    font-family: 'fontawesome';
    font-size: 14px;
    float: right;
  }
  /* button.accordion.is-open:after {
    content: '\f056';
  } */
  button.accordion:hover, button.accordion.is-open {
    background-color: #ddd;
    /* border: .1rem solid #ddd; */
  }
  .accordion-content {
    padding: 0 20px;
    border-left: 1px solid whitesmoke;
    border-right: 1px solid whitesmoke;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-in-out;

  }
  .accordion-content form {
    display: flex;
    flex-direction: row;
    margin: .2rem 10% .2rem 10%;
    justify-content: space-between;
  }
  .footer {
    width: 80%;
    height: 5px;
    margin-left: 10%;
    background-color: #ddd;
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    margin-bottom: .6rem;
  }
  button .btn-r {
    float: right;
    margin-right: 1rem;
    font-size: 1rem;
  }
  input[name="interval"] {
    color: black;
  }
  input[name="card"] {
    border: .1rem solid #ddd;
  }
  /* ---------------Titre + Hr ------------------*/
  .titreHr {
    width: 100%;
    text-align: left;
    padding: 0;
    margin: 0 2.5% 0 2.5%;
  }
  .titreHr span {
    color: blueviolet;
    font-weight: 600;
    margin-left: 2.5%;
  }
  .titreHr hr {
    width: 95%;
    height: .4rem;
    padding: 0;
    margin: .7rem 0 0 0;
    line-height: 1;
    letter-spacing: 0;
    border-top: 1px solid blueviolet;
  }
  /* ---------------Toutes Tables ------------------*/
  .table {
    width: 90%;
    margin-left: 5%;
    text-align: justify;
  }
  .table th {
    border: 0 solid white;
  }
  .table td {
    border-top: 0 solid white;
  }
  .form-control.url {
    width: 450px;
  }

  #count td  {
    border-style: solid 1px whitesmoke;
  }
  #result td:nth-of-type(3) {
    border-top-left-radius: 20px; 
    border-bottom-left-radius: 20px;
    border-top-right-radius: 20px;
    border-bottom-right-radius: 20px;
    /* background-color: lightcyan; */
  }
  /* ---------------Table CAPTURE ------------------*/
  .capture {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    overflow: hidden; 
  }
  .capture input {
    margin: 0 1rem .5rem  0; 
  }
  /* ---------------Table CAPTURE Responsive------------------*/
  @media screen and (max-width: 660px) {
    .capture input[name="urlCapt"] {
      width: 250px;
    }
  }
  /* ---------------Table LISTE ------------------*/
  .listeTh {
    width: 90%;
    min-width: 675px;
    /* justify-content: center; */
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: space-between;
    margin-left: 5%;
    margin-top: .5rem;
    font-weight: 600;
  }
  .listeTh span {
    margin-left: 200px;
  }
  .listeTd {
    width: 90%;
    min-width: 680px;
    /* justify-content: center; */
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: space-between;
    margin-left: 5%;
  }
  .listeUrl {
    display: flex;
    flex-direction: row;
    justify-content: start;
    margin-bottom: 1rem;
  }
  .listeActions {
    display: flex;
    flex-direction: row;
  }
  .listeActions span {
    margin-left: .5rem;
  }
  .listeUrl input {
    margin-right: .8rem;
  }
  .thLsUrl {
    margin-left: 0;
  }
  .tdLsAction {
    /* width: 30%; */
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: space-between;
    margin-left: 1rem;
  }
  .tdLsAction input {
    margin: 0 5%;
  }
  .thLsAction {
    /* width: 30%; */
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: space-between;
    margin-left: 1rem;
  }

  /* ---------------Table LISTE Responsive------------------*/
  @media screen and (max-width: 660px) {
    .listeUrl {
      margin-bottom: 0;
    }
    .thLsUrl {
      word-wrap: break-word;
    }
    .listeTh {
      /* min-width: 250px; */
      justify-content: start;
      flex-wrap: wrap;
    }
    .listeTh span {
      margin-left: 100px;
    }
    .listeTd {
      min-width: 150px;
      flex-wrap: wrap;
      text-align: justify;
      word-wrap: break-word;
    }
    .listeActions {
      margin-bottom: 2rem;
    }
    .thLsAction {
      display: none;
    }
    .tdLsAction input {
    margin: 0 20%;
  }
  }
  /* ---------------Table UrlData ------------------*/
  .table.event {
    width: 90%;
    margin-left: 5%;
  }
  .table.events th {
    text-align: center;
    border-top: 0 solid white;
    padding-top: 0.5rem;
    padding-bottom: 0;
  }

  </style>

  <!-- <span>Favoris:</span><br />
    <a href="http://api.open-notify.org/astros.json">http://api.open-notify.org/astros.json</a><br />
    <a href="https://financialmodelingprep.com/api/majors-indexes?datatype=json">https://financialmodelingprep.com/api/majors-indexes?datatype=json</a><br />
    <a href="https://examples.opendatasoft.com/api/datasets/1.0/world-heritage-unesco-list/">https://examples.opendatasoft.com/api/datasets/1.0/world-heritage-unesco-list/</a><br /> -->
  <!-- <table class="table" id="count">
    <tr>
      <td>Total urls: <?php if(isset($sumUrls)) { echo $sumUrls; } ?></td> -->
      <!-- <td>Total events: <?php if(isset($totalEv)) { echo $totalEv; } ?></td> -->
    <!-- </tr>
  </table> -->
  <div class="titreHr">
    <span>Poster<span>
    <hr />
  </div>

  <table class="table poster">
    <tbody>
      <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" name="urlPost">
        <tr>
            <td>
              <div class="capture">
                <input type="submit" class="btn btn-primary btn-sm" name="capture" value="Capture" />

                <input type="text" class="form-control url" name="urlCapt" placeholder="Quel est le WebService ?" />
                <?php if (isset($existUrlSite)) { echo $existUrlSite; } ?>

                <label for="start"></label>
                <input type="date" class="btn btn-primary btn-sm" id="start" name="dateCapt" min="<?php if (isset($today)) { echo $today; } ?>" max="2021-08-31" value="<?php echo $today; ?>" required  />
                <span class="validity"></span>
              </div>
            </td>
        </tr>
      </form>
      </tbody>
  </table>

  <div class="titreHr">
    <span>Liste<span>
    <hr />
  </div>
    <div class="listeTh">
        <div class="listeUrl" >
          Editer
        <span>Url</span>
        </div>
        <div class="listeActions">
          <div class="thLsAction">
            <span>Départ<span>
            Interval
          </div>
          <div class="thLsAction">
            Actions
          </div>
        </div>
    </div>

      <?php 
      if (isset($resCapt)) {
        foreach ($resCapt as $key => $value) {
          $urlsIDVal = $resCapt[$key]->{'urlsID'};
          $urls = json_decode($resCapt[$key]->{'urls'});
          $beginDate = $urls->{'beginDate'};
          $startDate = date('d/m/y', strtotime($beginDate));
          $urlName = $urls->{'url'};
          $valueEv = ($urls->{'urlStatus'} == false) ? 'on' : 'off';
          $classEv = ($valueEv == 'on') ? 'btn btn-success btn-sm' : 'btn btn-warning btn-sm';
          $intervalEv = $urls->{'interval'};
          $interval = (isset($intervalEv) == true) ? octal(floor($intervalEv / 60)) . ':' . octal($intervalEv % 60) : '00:00';
          $valueTime = ($interval == null) ? 'value="00:01"' : 'value="' . $interval . '"';
          $intervalType = ($valueEv == 'off') ? 'hidden' : 'time';
          $intervalInfo = ($valueEv == 'on') ? 'hidden' : 'submit';
      ?>
        <div class="listeTd">
          <div class="listeUrl" >
            <form method="post" name="evInfo" action="<?php echo htmlspecialchars($path . '/home/create.php'); ?>">
              <input type="submit" name="card" value="info" class="btn btn-light btn-sm" />
              <input type="hidden" name="urlsPost" value="<?php if (isset($urlsIDVal)) { echo $urlsIDVal; } ?>" />
              <input type="hidden" name="urlName" value="<?php if (isset($urlName)) { echo $urlName; } ?>" />
            </form>
            <span class="thLsUrl"><?php if (isset($urlName)) { echo $urlName; } ?></span>
          </div>

          <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" name="evPost">
            <div class="listeActions">
                <span><?php if (isset($startDate)) { echo $startDate; } ?></span>
              <div class="tdLsAction">
                <label for="startTime"></label>
                <input type="<?php if(isset($intervalType)) { echo $intervalType; } ?>" class="btn btn-primary btn-sm" id="startTime" name="intervalEv" <?php if (isset($valueTime)) { echo $valueTime; } ?> required />
                <input type="<?php if(isset($intervalInfo)) { echo $intervalInfo; } ?>" class="btn btn-info btn-sm" name="interval" value="<?php if (isset($interval)) { echo $interval; } ?>" disabled />

                <!-- <input type="submit" class="btn btn-success" value="&radic;" /> -->
                <!-- <span class="validity"></span> -->
                <!-- <p><span id="value">n/a</span></p> -->

              </div>
              <div class="tdLsAction">
                <input type="submit" class="<?php if(isset($classEv)) { echo $classEv; } ?>" name="<?php if (isset($valueEv)) { echo $valueEv; } ?>" value="<?php if (isset($valueEv)) { echo $valueEv; } ?>" />
                <input type="hidden" name="urlsPost" value="<?php if (isset($urlsIDVal)) { echo $urlsIDVal; } ?>" />
                <input type="hidden" name="urlName" value="<?php if (isset($urlName)) { echo $urlName; } ?>" />
                <input type="submit" class="btn btn-danger btn-sm" name="deleteUrl" value="x" />
               </div>
            </div>
          </form>
      </div>
    <?php
      }
    }
    ?>

  <div class="titreHr">
    <span>UrlData<span>
    <hr />
  </div>
  <div class="dasboard">
    <table class="table events">
      <tr>
        <th>Events</th>
        <th>Lien</th>
        <th>actions</th>
      </tr>
    </table>
    <?php
    if (isset($arrUrlCat)) {
      foreach ($arrUrlCat as $key => $value) {
        $urlNameUnique = $arrUrlCat[$key];
        ?>
        <div>
          <button class="accordion"><span class="btn-l"><?php echo $urlNameUnique; ?></button>
          <div class="accordion-content">
        <?php
        // var_dump($newsEv);
        foreach ($newsEv as $key => $value) {
          $urlName = $newsEv[$key]->{'urlName'};
          if ($urlName == $urlNameUnique) {
            $beginEv = $newsEv[$key]->{'beginEv'};
            $endEv = $beginEv;
            $endTime = $beginEv;
            $datetime = new DateTime($beginEv);
            $sem = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
            $jSem = $datetime->format('w');
            $dateEv = $datetime->format('d/m/Y');
            $timeEv = $datetime->format('H:m:s');
            $intervalEv = $newsEv[$key]->{'intervalEv'};
            // if ($newsEv[$key]->{'statusEv'} === true) {
              $eventID = $newsEv[$key]->{'eventID'};
              ?>
                <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
                  <span><?php echo $eventID; ?></span>
                  </span><span class="btn-r"><?php echo $dateEv; ?> à <?php echo $timeEv; ?></span>
                  <span><a href="../api/apiFinancial.php" target="blank">new link</a></span>
                  <input type="submit" class="btn btn-danger btn-sm" name="deleteEvent" value="x" />
                  <input type="hidden" name="eventID" value="<?php if (isset($eventID)) { echo $eventID; }?>" />
                </form>
              
            <?php
            // }
          }
        }
        ?>
          </div>
        <div class="footer"></div>
      </div>
      <?php
      }
    }
    ?>
  <nav aria-label="nav">
    <ul class="pagination justify-content-center">
      <li <?php echo $disabledPrev; ?>>
        <a class="page-link" href="?page=<?php echo $current -1; ?>" <?php echo $tabindexPrev; ?> >Previous</a>
      </li>
      <?php foreach ($arrDh as $key => $value) {
      ?>
      <li <?php echo $arrDh[$key]->{'class'}; ?>>
        <a class="page-link" href="?page=<?php echo $arrDh[$key]->{'pageIndex'}; ?>"><?php echo $arrDh[$key]->{'pageIndex'}; ?><?php echo $arrDh[$key]->{'span'}; ?></a>
      </li>
      <?php
      }
      ?>
      <li <?php echo $disabledNext; ?>>
        <a class="page-link" href="?page=<?php echo $current +1; ?>" <?php echo $tabindexNext; ?> >Next</a>
      </li>
    </ul>
  </nav>
</div>
<script>
  // var labels = document.getElementsByTagName('LABEL');
  // for (var i = 0; i < labels.length; i++) {
  //     if (labels[i].htmlFor != '') {
  //         var elem = document.getElementById(labels[i].htmlFor);
  //         if (elem)
  //             elem.label = labels[i];         
  //     }
  // }
  // var startTime = document.getElementById('startime').label.innerHTML = 'Look ma this works!';

  var startTime = document.getElementById("startTime");
  var valueSpan = document.getElementById("value");

  if (startTime) {
    startTime.addEventListener("input", function() {
      valueSpan.innerText = startTime.value;
    }, false);
  }

  // var dateControl = document.querySelector('input[type="date"]');
  // dateControl.value = '2019-08-31';

  // var timeControl = document.querySelector('input[type="time"]');
  // timeControl.value = '00:01';

  var accordions = document.getElementsByClassName('accordion');
    for (var i=0; i < accordions.length; i++){
        accordions[i].onclick = function() {
            this.classList.toggle('is-open');

            var content = this.nextElementSibling;
            if (content.style.maxHeight){
                // accordion isest ouvert
                content.style.maxHeight = null;
            } else {
                // accordion est fermer
                content.style.maxHeight = content.scrollHeight + 'px';
            };
        };
    };

</script>
<?php

require_once '../home/footer.php';

?>