<?php

// require_once '../vendor/autoload.php';
require_once 'header.php';
// require_once '../controller/perso.php';


?>
<style>
.espace {
  margin: 10px;
}
.espace img {
  border-bottom: .1rem solid #ddd;
}
/*---------Card -----------*/
div .card-body {
  padding: .4rem;
  height: 200px;
}
.card-text {
  min-height: 100px;
  max-height: 130px;
  margin-bottom: 0;
  padding-bottom: 0;
}
/*---------Modal -----------*/
.modal.fade2{
  width: 25%;
  margin-left: 15%;
  overflow: visible;
}
.modalCard-title {
  font-size: 2rem;
  margin: 1rem;
}
.modalCard-text {
  min-height: 100px;
  max-height: 100%;
  margin-bottom: 0;
  padding-bottom: 0;
}
.modalCard-link {
  padding: 1rem;
}
</style>
<header class="header">
  <div id="sideNavigation" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()" style="text-decoration:none;">&times;</a>
      <!-- <span>Vos 3 dernièrs événements</span> -->
      <?php

      foreach ($newsPerso as $key => $value) {
        if ($key < 4) {
          $idBg = $newsPerso[$key]->{'id'};
          $titreBg = $newsPerso[$key]->{'titre'};
          $contenuBg = $newsPerso[$key]->{'contenu'};
          $urlDateAj= $newsPerso[$key]->{'dateAjout'};
          $urlDateMod= $newsPerso[$key]->{'dateModif'};
          $urlNomBg = $newsPerso[$key]->{'urlName'};
          $urlCatBg = $newsPerso[$key]->{'categorie'};
          $imgNomBg = $newsPerso[$key]->{'imgNom'};

          $datetime = new DateTime($urlDateMod);
          // echo $datetime->format('Y');
          $sem = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
          $jSem = $datetime->format('w');
          $dateMod = $datetime->format('d/m/Y');
          $timeMod = $datetime->format('H:m:s');
        ?>
        <div id="bulle">
          <p style="text-align:center;"><img src="../uploads/<?php echo $imgNomBg; ?>" style="width:100px" /></p>
          <p>Titre: <span><?php if (isset($titreBg)) { echo $titreBg; }?></span></p>
          <p>Actualisé le : <span><?php if (isset($dateMod)) { echo $dateMod; }?></span></p>
          <p>à : <span class="timeMod"><?php if (isset($timeMod)) { echo $timeMod; }?></span></p>
          <p>catégorie :<span class="catMod"><?php if (isset($urlCatBg)) { echo $urlCatBg; }?><span></p>
        </div>
        <?php
        }
      }
      ?>
  </div>
  <nav class="topnav">
    <a href="#" onclick="openNav()">
      <img src="../../assets/menu_lateral.svg" alt="iconMenu" title="menuLateral" />
    </a>
  </nav>

</header>
<main id="main">
  <div class="cardCat">
    <?php
    foreach ($newsPerso as $key => $value) {
      $idBg = $newsPerso[$key]->{'id'};
      $titreBg = $newsPerso[$key]->{'titre'};
      $contenuBg = $newsPerso[$key]->{'contenu'};
      $urlNomBg = $newsPerso[$key]->{'urlName'};
      $urlCatBg = $newsPerso[$key]->{'categorie'};
      $imgNomBg = $newsPerso[$key]->{'imgNom'};
      $imgTypeBg = $newsPerso[$key]->{'imgType'};
      $resCount = $newsPerso[$key];
      $idModal = 'myModal' . $idBg;

      ?>
      <div class="card" data-toggle="modal" data-target="#<?php echo $idModal; ?>">
        <img src="../uploads/<?php if(isset($imgNomBg)) { echo $imgNomBg; } ?>" class="card-img-top" alt="photoRandom">
        <div class="card-body">
          <h6 class="card-title"><?php if (isset($titreBg)) { echo debutTexte($titreBg, 50); }?></h6>
          <p class="card-text" ><?php if (isset($contenuBg)) { echo debutTexte($contenuBg, 150); }?></p>
          <span>API :</span>
          <a href="../api/apiAstros.php" title="Actualisé le: <?php if (isset($dateEv)) { echo $dateEv; } ?> à <?php if (isset($timeEv)) { echo $timeEv; }?>" class="card-link">lien</a>
          <a href="../api/astros.json" title="Actualisé le: <?php if (isset($dateEv)) { echo $dateEv; }?> à <?php if (isset($timeEv)) { echo $timeEv; }?>"  class="card-link"><img src="../img/download.svg" /></a>
          <a href="<?php if (isset($urlNomBg)) { echo $urlNomBg; }?>" title="Temps réel / Libre"class="card-link">web</a>
        </div>
      </div>

      <div class="modal fade2" id="<?php echo $idModal; ?>">
        <div class="modalCard">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <img src="../uploads/<?php if(isset($imgNomBg)) { echo $imgNomBg; } ?>" class="card-img-top" alt="photoRandom">
          <div class="modalCard-body">
            <h5 class="modalCard-title"><?php if (isset($titreBg)) { echo $titreBg; }?></h5>
            <p class="modalCard-text" ><?php if (isset($contenuBg)) { echo $contenuBg; }?></p>
            <span>API :</span>
            <a href="../api/apiAstros.php" title="Actualisé le: <?php if (isset($dateEv)) { echo $dateEv; } ?> à <?php if (isset($timeEv)) { echo $timeEv; }?>" class="modalCard-link">lien</a>
            <a href="../api/astros.json" title="Actualisé le: <?php if (isset($dateEv)) { echo $dateEv; }?> à <?php if (isset($timeEv)) { echo $timeEv; }?>"  class="modalCard-link"><img src="../img/download.svg" /></a>
            <a href="<?php if (isset($urlNomBg)) { echo $urlNomBg; }?>" title="Temps réel / Libre"class="modalCard-link">web</a>
          </div>
        </div>
      </div>
    <?php
    }
    ?>
  </div>
</main>
<?php


require_once 'footer.php';

?>
