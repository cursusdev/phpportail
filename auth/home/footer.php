    <!-- <hr />
    <footer>
        <p>
            <?php echo $tr['footer_text'] ?>
        </p>
    </footer> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
    <script>
    window.cookieconsent.initialise({
        'container': document.getElementById("content"),
        'palette':{
        'popup': {background: "#90EE90"},
        'button': {background: "#aa0000"},
        },
        'revokable':true,
        'onStatusChange': function(status) {
        console.log(this.hasConsented() ?
        'enable cookies' : 'disable cookies');
        },
        'law': {
        'regionalLaw': false,
        },
        'location': true,
        'content': {
            header: 'Cookies utilisés sur le site!',
            message: 'Ce site utilise des cookies pour améliorer votre expérience.',
            dismiss: 'Je l\'ai!',
            allow: 'Autorise les cookies',
            deny: 'Decline',
            link: 'Apprendre encore plus',
            href: 'https://www.cookiesandyou.com',
            close: '&#x274c;',
            policy: 'Cookie Policy',
            target: '_blank',
        }
    });
    </script> -->
    <script>
      function openNav() {
          document.getElementById("sideNavigation").style.width = "215px";
          document.getElementById("main").style.marginLeft = "215px";
      }
      
      function closeNav() {
          document.getElementById("sideNavigation").style.width = "0";
          document.getElementById("main").style.marginLeft = "0";
      }
    </script>
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script> -->
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
    <script src="../../js/jquery-3.4.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
  </body>
</html>
