<?php

$lang = $fr_class = $en_class = '';
/* Récupération de la langue dans la chaîne get */
$lang = (isset($_GET['lang']) && file_exists('../../lang/'.$_GET['lang'].'.json')) ? $_GET['lang'] : 'fr';
/* Définition de la class pour les liens de langue */
if ($lang == 'fr')
    $fr_class = ' class="active"';
else
    $en_class = ' class="active"';
/* Récupération du contenu du fichier .json */
$contenu_fichier_json = file_get_contents('../../lang/'.$lang.'.json');
/* Les données sont récupérées sous forme de tableau (true) */
$tr = json_decode($contenu_fichier_json, true);


?>
<!DOCTYPE html>
<?php
ini_set('session.cookie_httponly', 1 );
session_start();
?>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="shortcut icon" type="image/ico" href="../../assets/favicon.ico">
      <meta name="viewport" content="width=device-width" />
      <title><?php echo $tr['head_title'] ?></title>
      <meta name="description" content="<?php echo $tr['head_description'] ?>" />
      <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
      <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css"> -->

      <link rel="stylesheet" href="../../styles/bootstrap.min.css" type="text/css" />
      <!-- <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script> -->
      
      <link rel="stylesheet" href="../../styles/auth.php" type="text/css" />
      <!-- <script type='text/javascript' src='http://www.bing.com/api/maps/mapcontrol?callback=GetMap&key=AgcU62H_r6VDjnZ7EUPxCod5twtVNg7oIp48rfsr2WKqbH8MYU95Zox9DhtYVlpS' async defer></script> -->
      <style>
      * {
        margin: 0;
        padding: 0;
        font-family: sans-serif;
        /* background-color: whitesmoke; */
      }
      body {
        margin: 0;
        padding: 0;
      }
      .services {
        font-size: 1.5rem;
        font-weight: 1.5rem;
        margin: 50px;
      }
      /* .astros {
        width: 50%;
        margin: 10px auto;
        padding: 5px;
        background-color: whitesmoke;
        border-radius: 10px;
      }
      .financial {
        width: 50%;
        margin: 10px auto;
        padding: 5px;
        background-color: whitesmoke;
        border-radius: 10px;
      }
      .unesco {
        width: 50%;
        margin: 10px auto;
        padding: 5px;
        background-color: whitesmoke;
        border-radius: 10px;
      } */
      /*--------------  Card -------------------*/
      .cardCat {
        display: flex;
        flex-flow: row;
        justify-content: center;
        align-items: flex-start;
        align-content: flex-start;
      }
      .card {
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        margin: 35px 5px 5px;
        max-width: 12rem;
        font-size: 0.8rem;
        align-self: auto;
      }
      .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
      }
      .card img {
        height: 8rem;
      }
      .card-link img {
        width: 1rem;
        height: 1rem;
      }
      .points {
        font-size: 20px;
        color: blue;
      }
      .card-text {
        min-height: 120px;
        margin-bottom: 0;
        padding-bottom: 0;
      }
      .cardCat {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
      }
      .card {
        min-width: 150px;
      }
      div .card-body {
        padding: .4rem;
      }
      /* -----------------------Menu horizontal------------------- */
      .navbar img {
        margin-right: .5rem;
      }
      .navbar {
        justify-content: center;
      }
      .navbar ul {
        text-justify: right;
        padding-left: 40px;
      }
      .nav-item a {
        padding: 0;
      }
      .navbar > ul > li:first-child {
        margin-right: 25px;
      }
      .dropdown-menu > a {
        padding-left: 1rem;
      }
      #idMenu.form-control {
        box-shadow: 0 0 .5em green;
      }
      /* --------------------Menu latéral -----------------------*/
      #sidebar {
          position: fixed;
          width: 200px;
          height: 100%;
          background: azure;
          left: 0;
          transition: all 500ms linear;
          margin-top: -10px;
      }
      #sidebar.active {
          left: 0;
      }
      #sidebar ul li {
          color: rgba(0,0,230,0.9);
          list-style: none;
          padding: 5px 10px;
          border-bottom: 1px solid rgba(100,100,100,0.3);
      }
      #sidebar .toggle-btn {
          position: absolute;
          left: 220px;
          top: 10px;
      }
      #sidebar .toggle-btn span {
          display: block;
          width: 30px;
          height: 5px;
          background: lightgrey;
          margin: 5px 0;
      }
      /* ---------------Menu latérale : bulles --------------------*/
      #sidebar form {
        margin: 1.5rem 0 1.5rem 0;
      }
      #sidebar span {
        font-size: 0.8rem;
      }
      .form-control {
        width: 190px;
        margin-left: 5px;
        font-size: 0.8rem;
      }
      #bulle {
        width: 190px;
        /* width: 100%; */
        background-color: white;
        border-radius: 10px;
        font-size: 0.8rem;
        margin: .2rem;
        padding: .5rem;
      }
      #bulle p {
        padding: 0;
        margin: 0;
        text-align: left;
      }
      #bulle span {
        font-size: 1rem;
        font-weight: 500;
      }
      .dateMod {
        /* margin-left: 12px; */
      }
      .timeMod {
        margin-left: 58px;
      }
      .catMod {
        margin-left: 16px;
      }
      /* ---------------Menu latérale --------------------*/
      /* The side navigation menu */
      .sidenav {
          height: 100%; /* 100% Full-height */
          width: 0; /* 0 width - change this with JavaScript */
          position: fixed; /* Stay in place */
          left: 0;
          background-color: orange; /* Black*/
          overflow-x: hidden; /* Disable horizontal scroll */
          padding-top: 60px; /* Place content 60px from the top */
          transition: 0.5s; /* 0.5 second transition effect to slide in the sidenav */
          z-index: 2;
      }
      /* When you mouse over the navigation links, change their color */
      .sidenav a:hover, .offcanvas a:focus{
          color: #f1f1f1;
      }
      
      /* Position and style the close button (top right corner) */
      .sidenav .closebtn {
          position: absolute;
          top: 0;
          right: 25px;
          font-size: 36px;
          margin-left: 50px;
      }
      
      /* Style page content - use this if you want to push the page content to the right when you open the side navigation */
      #main {
          transition: margin-left .5s;
          padding: 20px;
          overflow:hidden;
          width:100%;
      }
      body {
        overflow-x: hidden;
      }
      
      /* Add a black background color to the top navigation */
      .topnav {
          /* background-color: #333; */
          overflow: hidden;
      }
      
      /* Style the links inside the navigation bar */
      .topnav a {
          float: left;
          display: block;
          color: #f2f2f2;
          text-align: center;
          padding: 14px 16px;
          text-decoration: none;
          font-size: 17px;
      }
      
      /* Change the color of links on hover */
      .topnav a:hover {
          background-color: #ddd;
          color: black;
      }
      /*-------------- Modal -------------*/
      .modal {
        top: 10%;
        bottom:10%;
        left: 20%;
        right: 20%;
        /* background-color: white; */
        
      }
      .card button {
        margin-left: 160px;
      }
      .modalCard {
        max-width: 12rem;
        font-size: 1.2rem;
        align-self: auto;
      }

      .modalCard img {
        height: 18rem;
        border-top-right-radius: 15px;
        border-top-left-radius: 15px;
      }
      .modalCard-link img {
        width: 1rem;
        height: 1rem;
      }
      .points {
        font-size: 20px;
        color: blue;
      }
      .modalCard-text {
        min-height: 320px;
        margin-bottom: 0;
        padding-bottom: 0;
      }
      .cardCat {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
      }
      .modalCard {
        min-width: 98%;
        height: 95%;
      }
      div .modalCard-body {
        padding: .4rem;
        background-color: white;
        border-bottom-right-radius: 15px;
        border-bottom-left-radius: 15px;
      }

    </style>
  </head>
  <!-- <body  onload="GetMap();"> -->
  <body>
<?php

$_SESSION['username'] = 'cursus';

require_once '../env.php';

$host = Env::get_host();
$path = Env::get_path_auth();
$today = date('Y-m-d', strtotime('now'));
$_SESSION['siteID'] = 1;


require_once '../auth_cookie.php';  //page profil
require_once '../controller/pdo.php';
require_once '../controller/utiles.php';
require_once '../controller/_menus.php';
require_once '../controller/_accueil.php';
require_once '../controller/_espace.php';
require_once '../controller/_dashboard.php';
require_once '../controller/_create.php';
include 'menus.php';


// if(isset($_COOKIE['auth_cookie'])) {
//     if(isset($_SESSION['role']) && $_SESSION['role'] == 'admin') {
//       // echo 'Tu est connecté en tant qu\'administrateur';
//     }
// } else {
//     header('location: ' . $path . '/login.php');
// }