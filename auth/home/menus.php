<?php

require_once 'header.php';
// require_once '../controller/_menus.php';


// echo htmlspecialchars($_SERVER['PHP_SELF']);

?>
<nav class="navbar navbar-expand-sm navbar-light bg-light">
  <img src="../../assets/logo.png" alt="logo" title="logo"/>
  <h4 tabindex="0" ><?php echo $tr['site_h1']; ?></h4>
  <ul class="navbar-nav mr-auto" role="menu">
    <li class="nav-item <?php echo $activeAccueil; ?>">
      <a class="nav-link" href="<?php echo $path; ?>/home/accueil.php" tabindex="1"><?php echo $tr['onglet1']; ?></a>
    </li>
    <li class="nav-item <?php echo $activePerso; ?>">
      <a <?php echo $classUser; ?><?php echo $hrefPerso; ?> tabindex="5">Espace</a>
    </li>
    <li class="nav-item <?php echo $activeDashboard; ?>">
        <a <?php echo $classUser; ?><?php echo $hrefDashboard; ?> tabindex="6">Nouveau</a>
    </li>
    <li class="nav-item <?php echo $activeCreate; ?>">
      <a <?php echo $classUser; ?><?php echo $hrefCreate; ?> tabindex="7">Créer</a>
    </li>
    <li class="nav-item dropdown" role="menu">
      <a class="nav-link dropdown-toggle" href="../accounts.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" <?php echo $activeCompte; ?> tabindex="8">Compte</a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown" >
           <form id="close" method="post" action="../accounts.php">
            <input type="hidden" name="close" value="close" />
          </form>
          <a class="dropdown-item" href="#" onclick="document.getElementById('close').submit()" tabindex="6">Déconnecter</a>
      </div>
    </li>
  </ul>
  <form name="PostName" method="post">
    <select class="form-control" name="idMenu" id="idMenu" onchange="PostName.submit()">
      <option value="choix" <?php $menuSelected("choix"); ?>>Recherche catégorie</option>
      <?php foreach ($arrCat as $key => $value) { ?>
        <option value="<?php echo $arrCat[$key]; ?>" <?php $menuSelected($arrCat[$key]); ?>>
          <?php echo $arrCat[$key]; ?>
        </option>
        <?php } ?>
    </select>
    <input type="hidden" name="valueIndex" value=""/> 
  </form>
</nav>

<script>
  var object = document.forms[2].idMenu;
  var index = object.selectedIndex;
  var valueIndex =  object.options[index].value;
  var valueControl = document.querySelector('input[name="valueIndex"]');
  valueControl.value = valueIndex;
</script>
<?php


require_once 'footer.php';

?>