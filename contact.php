<?php

require_once 'head.php';

?>
<section class="mb-4">
  <h2 class="h1-responsive font-weight-bold text-center my-4"><?php echo $tr['contact_title']; ?></h2>
  <p class="text-center w-responsive mx-auto mb-5"><?php echo $tr['contact_content1']; ?><br /><?php echo $tr['contact_content2']; ?></p>

  <div class="contact">
    <ul class="list-unstyled mb-0">
      <li><i class="fas fa-envelope mt-4 fa-2x"></i>
        <p><?php echo $tr['contact_coord']; ?></p>
      </li>
      <li><i class="fas fa-map-marker-alt fa-2x"></i>
        <p><?php echo $tr['contact_city']; ?></p>
      </li>
    </ul>
  </div>
  <br />
  <div class="row">
    <div class="col-md-9 mb-md-0 mb-5">
      <form id="contact-form" name="contact-form" action="contact.php" method="POST">
        <div class="row">
          <div class="col-md-6">
            <div class="md-form mb-0">
                <label for="name" class=""><?php echo $tr['contact_nom']; ?></label>
                <input type="text" id="name" name="name" class="form-control">
            </div>
          </div>
          <div class="col-md-6">
            <div class="md-form mb-0">
                <label for="email" class=""><?php echo $tr['contact_email']; ?></label>
                <input type="text" id="email" name="email" class="form-control">
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="md-form mb-0">
                    <label for="subject" class=""><?php echo $tr['contact_object']; ?></label>
                    <input type="text" id="subject" name="subject" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12">
              <div class="md-form">
                  <label for="message"><?php echo $tr['contact_message']; ?></label>
                  <textarea type="text" id="message" name="message" rows="6" class="form-control md-textarea"></textarea>
              </div>
          </div>
        </div>
      </form>
      <div class="text-center text-md-left">
        <a class="btn btn-success" onclick="validateForm()"><?php echo $tr['send']; ?></a>
      </div>
      <span id="status"></span>
    </div>
  </div>
</section>
<?php


require_once 'foot.php';

?>