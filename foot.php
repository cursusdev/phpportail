
    <footer id="gluant">
        <?php echo $tr['footer_text'] ?>
    </footer>

    <script>
      function validateForm() {
        let name =  document.getElementById('name').value;
        if (name == "") {
            document.getElementById('status').innerHTML = "<?php echo $tr['resp_name']; ?>";
            return false;
        }
        let email =  document.getElementById('email').value;
        if (email == "") {
            document.getElementById('status').innerHTML = "<?php echo $tr['resp_email1']; ?>";
            return false;
        } else {
            let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(!re.test(email)){
                document.getElementById('status').innerHTML = "<?php echo $tr['resp_email2']; ?>";
                return false;
            }
        }
        let subject =  document.getElementById('subject').value;
        if (subject == "") {
            document.getElementById('status').innerHTML = "<?php echo $tr['resp_object']; ?>";
            return false;
        }
        let message =  document.getElementById('message').value;
        if (message == "") {
            document.getElementById('status').innerHTML = "<?php echo $tr['resp_message']; ?>";
            return false;
        }
        document.getElementById('status').innerHTML = "<?php echo $tr['resp_send2']; ?>";
        document.getElementById('contact-form').submit();
      }
    </script>
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
