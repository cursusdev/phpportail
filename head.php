<?php
$lang = $fr_class = $en_class = '';
/* Récupération de la langue dans la chaîne get */
$lang = (isset($_GET['lang']) && file_exists('lang/'.$_GET['lang'].'.json')) ? $_GET['lang'] : 'fr';
/* Définition de la class pour les liens de langue */
if ($lang == 'fr')
    $fr_class = ' class="active"';
else
    $en_class = ' class="active"';
/* Récupération du contenu du fichier .json */
$contenu_fichier_json = file_get_contents('lang/'.$lang.'.json');
/* Les données sont récupérées sous forme de tableau (true) */
$tr = json_decode($contenu_fichier_json, true);

?>
<!DOCTYPE html>
<?php 
ini_set('session.cookie_httponly', 1 );
session_start() 
?>
<html>
  <head>
    <meta charset="uft-8">
    <title><?php echo $tr['head_title'] ?></title>
    <link rel="shortcut icon" type="image/ico" href="assets/favicon.ico">
    <meta name="description" content="<?php echo $tr['head_description'] ?>" />
    <link rel="stylesheet" href="styles/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="styles/styles.php" type="text/css" />
  </head>
  <body>

  <?php
  require_once __DIR__ . './auth/env.php';

  $host = Env::get_host();
  $path = Env::get_path();

  require_once 'main.php';

  $activeAccueil = ($_SERVER['REQUEST_URI'] == $host . $path . '/index.php') ? 'active' : '';
  $activeContact = ($_SERVER['REQUEST_URI'] == $host . $path . '/contact.php') ? 'active' : '';

  ?>
  <nav class="navbar navbar-expand-sm navbar-light bg-light" id="bandeau">
    <img src="assets/logo.png" alt="logo" title="logo"/>
    <h4 tabindex="0"><?php echo $tr['site_h1']; ?></h4>
    <ul class="navbar-nav mr-auto" role="menu">
      <li class="nav-item">
        <a class="nav-link" href="./index.php" tabindex="1"><?php echo $tr['onglet1']; ?></a>
      </li>
      <li class="nav-item">
      <a class="nav-link" href="./contact.php" tabindex="2"><?php echo $tr['onglet2']; ?></a>
      </li>
    </ul>
    <div>
      <a <?php echo $en_class ?> href="?lang=en"><img src="assets/en.png" alt="Français" /></a> <a <?php echo $fr_class ?> href="?lang=fr"><img src="assets/fr.png" alt="Anglais" /></a>
    </div>
  </nav>