<?php

require_once 'head.php';

// $_SESSION['siteName'] = 'Monsite';
// echo $_SESSION['siteName'];
?>
<style>

 
  /* ---------------Page index ------------------*/
  #presentation {
    width: 200px;
  }
  #comment-box {
    height: 40%;
  }
  #forfait-card1 {
    width: 100%;
    background-color:lightcoral;
    font-size: 14px;
    float: left;
    margin-bottom: 5px;
  }
  #forfait-card1 table {
      margin: auto auto 10px auto;
      background-color: #FED6CD;
      border-radius: 7px;
  }
  #forfait-card1 td {
      padding: 2px;
      font-size: 12px;
      text-align: left;
  }
  .disabled {
    text-align: center;
  }
  #comment-box table {
    margin: auto;
  }
  #comment-box td {
    margin: 5px;
  }
  #explain1, #explain2, #explain3 {
    width: 90%;
    height: 50%;
    margin: 2rem;
    display: inline-block;
  }
  #explain1 #gauche {
    width: 40%;
    float: left;
    margin-top: 2rem;
    margin-left: 3rem;
  }
  #explain1 img {
    width: 100%;
  }
  #explain1 #droite {
    margin-left:60%;
  }
  #explain2 #gauche {
    margin-left:60%;
    margin-top: 2rem;
  }
  #explain2 #droite {
    width: 50%;
    float: left;
  }
  #explain2 img {
    width: 80%;
  }
  #explain3 {
  }
  #explain3 #gauche {
    width: 50%;
    float: left;
  }
  #explain3 #droite {
    margin-left:60%;
  }
  #compte1 {
    width: 90%;
  }
  #compte1 td {
    font-size: 1rem;
  }
  #compte1 td:first-child {
    padding-left: 1rem;
    text-align: center;
  }
  .card-body {
    padding-bottom: 0.5rem;
  }
  .card-body input {
    margin-top: 0.5rem;
  }

  @media screen and (max-width: 660px) {
    /* #explain1, #explain2 {
      width: 90%;
      margin: 1rem;
      display: inline;
      text-align:center;
    }
    #explain1 #gauche {
      width: 50%;
      float: none;
      margin-top: 2rem;
    }
    #explain2 #droite {
      width: 50%;
      float: none;
    } */
  }

</style>

<img src="assets/administration.jpg" title="APIsCommunautaaire" alt="adminsistration" />
<header>
    <article>
        <?php echo $tr['site_title']; ?>
        <?php echo $tr['site_descr1'] ?>
    </article>
    <h5><?php echo $tr['site_descr2'] ?></h5>
    <form action="./auth/login.php" method="POST">
        <button class="btn btn-info" name="login" ><?php echo $tr['login'] ?></button>
    </form>
    <hr />
</header>

<div class="titreHr">
    <!-- <span>Présentation<span> -->
    <hr />
  </div>

<main>
  <div id="explain1">
    <div id="gauche">
      <h4><?php echo $tr['content1_title1']; ?></h4>
      <p><?php echo $tr['content1_descr1']; ?></p>
      <p><?php echo $tr['content1_descr2']; ?></p>
    </div>
    <div id="droite">
      <img src="assets/administration1.jpg" title="APIEnrichissement" alt="adminsistration1" />
    </div>
  </div>

  <div class="titreHr">
    <hr />
  </div>

  <div id="explain2">
    <div id="droite">
      <img src="assets/administration2.jpg" title="APIEnrichissement" alt="adminsistration2" />
    </div>
    <div id="gauche">
      <h4><?php echo $tr['content2_title1']; ?></h4>
      <p><?php echo $tr['content2_descr1']; ?></p>
      <p><?php echo $tr['content2_descr2']; ?></p>
    </div>
  </div>

  <div class="titreHr">
    <hr />
  </div>

  <div id="explain3">
    <div id="gauche">
      <div class="table" id="comment-box">
        <h4><?php echo $tr['content3_title1']; ?></h4>
        <table>
          <tr>
            <td>Etape 1</td>
            <td><?php echo $tr['content3_descr1']; ?></td>
          </tr>
          <tr>
            <td>Etape 2</td>
            <td><?php echo $tr['content3_descr2']; ?></td>
          </tr>
          <tr>
            <td>Etape 3</td>
            <td><?php echo $tr['content3_descr3']; ?></td>
          </tr>
          <tr>
            <td>Etape 4</td>
            <td><?php echo $tr['content3_descr4']; ?></td>
          </tr> 
        </table>
      </div>
    </div>
    <div id="droite">
      <div class="card" id="forfait-card1">
        <div class="card-body">
          <h5 class="card-title"><?php echo $tr['content4_title1']; ?></h5>
          <p class="card-text"><?php echo $tr['content4_descr1']; ?></p>
          <table id="compte1">
            <tr>
              <td>5</td>
              <td><?php echo $tr['content4_descr2']; ?></td>
            </tr>
            <tr>
              <td>100</td>
              <td><?php echo $tr['content4_descr3']; ?></td>
            </tr>
            <tr>
              <td>1</td>
              <td><?php echo $tr['content4_descr4']; ?></td>
            </tr>
          </table>
          <form action="./auth/login.php" method="post">
            <input type="submit" class="btn btn-primary" name="gratuit" value="<?php echo $tr['inscription']; ?>" />
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
<?php


require_once 'foot.php';

?>