<?php


if ($_SERVER['REQUEST_URI'] == $path . '/index.php') {
  if ((isset($_POST['gratuit']) && !empty($_POST['gratuit']) || isset($_POST['premium']) && !empty($_POST['premium']))) {
    if (isset($_POST['gratuit'])) {
      $forfait = 'gratuit';
    }
    if (isset($_POST['premium'])) {
      $forfait = 'premium';
    }
    $_SESSION['forfait'] = $forfait;
    // Réalise un tableau de valeurs depuis fichier json pour réaliser des sessions 
    $arrForfait = startWebsite($forfait);
    $_SESSION['dataForfait'] = $arrForfait;

    // Réaliser un bouton annuler dans /form.php pout retour /index.php
    header('location: auth/home/accueil.php');
    exit();
  }
}


if ($_SERVER['REQUEST_URI'] == $path . '/contact.php' && isset( $_POST['name']) && isset( $_POST['email']) && isset( $_POST['message'])) {
  $name = htmlspecialchars($_POST['name']);
  $email = htmlspecialchars($_POST['email']);
  $subject = htmlspecialchars($_POST['subject']);
  $message = htmlspecialchars($_POST['message']);

  if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
    echo "Email format invalid.";
  };
  if (($name == '') || ($email == '')||($message == '')) {
    $mailEmpty = 'Veuillez remplir tous les champs de formulaire';
  } else {

    //   // Mode Production
    //   error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_STRICT);
    //   set_include_path("." . PATH_SEPARATOR . ($UserDir = dirname($_SERVER['DOCUMENT_ROOT'])) . "/pear/php" . PATH_SEPARATOR . get_include_path());
    //   // var_dump(get_include_path());

    //   // Mode developpement
    // require_once "C:/wamp/bin/php/php7.2.18/pear/PEAR.php";
    // require_once "C:/wamp/bin/php/php7.2.18/pear/Mail.php";
    // require_once "C:/wamp/bin/php/php7.2.18/pear/Net/SMTP.php";

    // $host = "n3plcpnl0158.prod.ams3.secureserver.net";
    // $username = "cursusdev@somprod.com";
    // $password = "cursus@2019";
    // $port = "465";

    // $from = $username;
    // $to = $email;
    // $subject = 'Hi!';

    // $body = "Hi,\n\nHow are you?";
    // $headers = array(
    //     'From' => $from,
    //     'To' => $to,
    //     'Subject' => $subject
    // );
    // $smtp = Mail::factory('smtp', array(
    //         'host' => $host,
    //         'port' => $port,
    //         'auth' => true,
    //         'username' => $username,
    //         'password' => $password
    //     ));
    // $mail = $smtp->send($to, $headers, $body);
    // if (PEAR::isError($mail)) {
    //   echo("<p>" . $mail->getMessage() . "</p>");
    // } else {
    //   echo $tr['resp_send1'];
    // }

  }
}

function startWebsite($forfait) {
  $dataGET = json_decode(file_get_contents('data/monsite.json'));
  $arrPack = ($dataGET->{'monsite'}->{'forfaits'});

  foreach ($arrPack as $key => $value) {
    if ($arrPack[$key]->{'abName'} == $forfait) {
      $dataSET = array(
        'siteName' => '',
        'abName' => $arrPack[$key]->{'abName'},
        'quotaUrls' => $arrPack[$key]->{'quotaUrls'},
        'quotaEv' => $arrPack[$key]->{'quotaEv'},
        'duration' => $arrPack[$key]->{'duration'}
      );
    }
  }
  return $dataSET;
}


?>