<?php
// indique au navigateur le contenu css
header('content-type: text/css');
// Mise en cache du CSS - empêche de charger systématiquement
// header('HTTP/1.0 304 Not Modified');
// contrôle de mise en cahce
header('Cache-Control: max-age=3600, must-revalidate');

session_start();
?>
body {
  font: 14px sans-serif;
  text-align: center;
}
#container {
  position: relative;
}
/* Page index.php */
#presentation {
    width: 200px;
}
.navbar {
  background-color:#6666ff!important;
  font-weight:500;
  font-size: 18px;
}
#card-box {
  margin-bottom: 15px;
  display: inline-block;
}
#comment-box {
}
#forfait-card1 {
  width: 18rem;
  background-color:lightcoral;
  font-size: 14px;
  float: left;
  height: 20rem;
  margin-bottom: 5px;
}
#forfait-card1 table {
  margin: auto auto 10px auto;
  background-color: #FED6CD;
  border-radius: 7px;
}
#forfait-card1 td {
  padding: 2px;
  font-size: 12px;
  text-align: left;
}
#forfait-card2 {
    width: 18rem;
    background-color:lightgreen;
    font-size: 14px;
    float: right;
    height: 20rem;
    margin-bottom: 5px;
}
#forfait-card2 table {
    margin: auto auto 5px auto;
    background-color: #CDFECF;
    border-radius: 7px;
}
#forfait-card2 td {
    padding: 2px;
    font-size: 12px;
    text-align: left;
}
.disabled {
    text-align: center;
}
#comment-box {
    font-size: 14px;
}
#comment-box table {
    margin: auto;
}
#comment-box td {
    margin: 5px;
}

/* Page form.php */
#input-box input {
    background-color: #CDFECF;
    text-align: center;
}
#input-box p {
    font-size: 10px;
    text-align: left;
    margin-left: 20px;
}
/* Page dashboard.php */
.dashboard td {
    width:100px;
}