<?php
// indique au navigateur le contenu css
header('content-type: text/css');
// Mise en cache du CSS - empêche de charger systématiquement
// header('HTTP/1.0 304 Not Modified');
// contrôle de mise en cahce
header('Cache-Control: max-age=3600, must-revalidate');

session_start();
?>
body {
  background: <?php echo $_SESSION['bg']['body']; ?>
  font: 14px sans-serif;
  text-align: center;
}
#container {
  background: <?php echo $_SESSION['bg']['container']; ?>;
  color: <?php echo $_SESSION['txt']['police']; ?>;
  border: 1px solid <?php echo $_SESSION['txt']['border']; ?>
  position: relative;
}
header {
  font-size: 20px;
  width: 60%;
  text-align: center;
  background-color: whitesmoke;
  margin-right: auto;
  margin-left: auto;
  margin-bottom: 2rem;
}
header article {
  margin-bottom: 2rem;
  padding: 2rem;
}

/* ---------------Titre + Hr ------------------*/
.titreHr {
  width: 100%;
  text-align: left;
  padding: 0;
  margin: 0 2.5% 0 2.5%;
}
.titreHr span {
  color: blueviolet;
  font-weight: 600;
  margin-left: 2.5%;
}
.titreHr hr {
  width: 95%;
  height: .4rem;
  padding: 0;
  margin: .7rem 0 0 0;
  line-height: 1;
  letter-spacing: 0;
  border-top: 1px solid blueviolet;
}

/* -----------------------Menu horizontal------------------- */
.navbar {
  background-color:#6666ff!important;
  font-weight:500;
  font-size: 18px;
}
.navbar img {
  margin-right: .5rem;
}
.navbar {
  justify-content: center;
}
.navbar ul {
  text-justify: right;
  padding-left: 40px;
}
.nav-item a {
  padding: 0;
}
.navbar > ul > li:first-child {
  margin-right: 25px;
}
.dropdown-menu > a {
  padding-left: 1rem;
}
#idMenu.form-control {
  box-shadow: 0 0 .5em green;
}

/* -----------------------Contact------------------- */
section {
  width: 80%;
  margin: 5% auto 0 auto;
  overflow: hidden;
}
.row {
  justify-content: center;
}
.contact {
  width: 75%;
  margin: 5% auto 0 auto;
  background-color: whitesmoke;
  border-radius: 10px;
  padding: 0.2rem;
}
form textarea {
  margin-bottom: 5%;
}
ul p {
  padding-top: 0.5rem;
  margin-bottom: 0.5rem;
}

/* -----------------------Footer------------------- */
#gluant {
  margin-top: 10%;
  width:100%;
  height:5%;
  padding: 1rem 2rem;
  background:#6666ff;
  color:white;
  font-weight:bold;
  font-size:12px;
  text-align:center;
  bottom:0;
  left:0;
}